package com.nomoreboundary.myrentalpro;

import java.util.Currency;
import java.util.Locale;

import android.app.Application;

public class MyRentalProApplication extends Application {

	private static final String TAG = "MyRentalProApplication";
	public static String MYRENTALPRO_SHARED_PREFERENCES = "com.nomoreboundary.myrentalpro.txt";
	private static Locale currentLocale = new Locale("us", "US");

	public static final String DATABASE_NAME = "property.db";
	public static final int DATABASE_VERSION = 4;


	
	public static String getCurrencySymbol() {
		Currency curr = Currency.getInstance("USD");
		return curr.getSymbol(currentLocale);
	}

}
