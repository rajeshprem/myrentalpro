package com.nomoreboundary.myrentalpro.util;

import android.annotation.SuppressLint;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

	@SuppressLint("SimpleDateFormat")
	public static int diffInMonths(String fromDate, String toDate) {
		
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		Calendar startCalendar = new GregorianCalendar();
		Calendar endCalendar = new GregorianCalendar();

		Date startDate;
		try {
			startDate = ft.parse(fromDate);
			startCalendar.setTime(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date endDate;

		try {
			endDate = ft.parse(toDate);
			endCalendar.setTime(endDate);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		int diffYear = endCalendar.get(Calendar.YEAR)
				- startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH)
				- startCalendar.get(Calendar.MONTH);

		return diffMonth;
	}
}
