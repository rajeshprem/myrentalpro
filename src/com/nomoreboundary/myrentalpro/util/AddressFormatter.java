package com.nomoreboundary.myrentalpro.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.location.Address;
import android.util.Log;

public class AddressFormatter {

	private final static String TAG = "AddressFormatter";

	/**
	 * split a string that looks like the android.location.Address toString()
	 * method
	 * 
	 * addressLines=[],admin=CA,sub-admin=null,thoroughfare="123 ABC St"
	 * 
	 * @param blobAddress
	 * @return
	 */
	public static String getFormattedAddress(String blobAddress) {

		/**
		 * splits into a list of strings based on commas addressLines=[]
		 * admin=[]
		 */
		List<String> addressParamList = Arrays.asList(blobAddress.split(","));

		/**
		 * Put everything in a map like ("thoroughfare", "123 ABC St")
		 */
		Map<String, String> addressMap = new HashMap<String, String>();
		for (String param : addressParamList) {
			List<String> sp = Arrays.asList(param.split("="));
			if (!sp.isEmpty()) {
				Log.d(TAG, "Extracted: " + sp.get(0) + "=" + sp.get(1));
				addressMap.put(sp.get(0), sp.get(1));
			}
		}

		/**
		 * Return something like 123 ABC St, Long Beach, CA 92127
		 */
		String formattedAddress = addressMap.get("thoroughfare") + ", "
				+ addressMap.get("locality") + " " + addressMap.get("admin")
				+ " " + addressMap.get("postalCode");

		Log.d(TAG, "Formatted Address = " + formattedAddress);
		return formattedAddress;
	}
	
	
	public static Address buildAddressObject(String blobAddress) {
		Address address = new Address(Locale.US);
		
		List<String> addressParamList = Arrays.asList(blobAddress.split(","));

		Map<String, String> addressMap = new HashMap<String, String>();
		for (String param : addressParamList) {
			List<String> sp = Arrays.asList(param.split("="));
			if (!sp.isEmpty()) {
				addressMap.put(sp.get(0), sp.get(1));
			}
		}
		
		address.setThoroughfare(addressMap.get("thoroughfare"));  //holds unit info
		address.setAdminArea(addressMap.get("admin"));
		address.setPostalCode(addressMap.get("postalCode"));
		address.setLocality(addressMap.get("locality"));
		
		return address;
	}

}
