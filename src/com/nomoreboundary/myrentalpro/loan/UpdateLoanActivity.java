package com.nomoreboundary.myrentalpro.loan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.LoanDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Loan;

public class UpdateLoanActivity extends Activity {

	private static final String TAG = "UpdateLoanActivity";
	private String mPropertyId;
	
	EditText mPurchasePrice, mInterestRate, mDownPayment, mTerm, mHoa;
	private Loan mLoan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_loan);
		// Show the Up button in the action bar.
		setupActionBar();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		Log.d(TAG, "onCreate() mPropertyId=" + mPropertyId);

		mPurchasePrice = (EditText) findViewById (R.id.edtPurchasePrice);
		mInterestRate = (EditText) findViewById (R.id.edtInterestRate);
		mDownPayment = (EditText) findViewById (R.id.edtDownPayment);
		mTerm = (EditText) findViewById (R.id.edtTerm);
		mHoa = (EditText) findViewById (R.id.edtHoa);
		
		updateLoanFields();
	}

	// Update loan fields if a record already exists
	private void updateLoanFields() {
		LoanDatabaseHelper dbHelper = new LoanDatabaseHelper(this);

		if (mPropertyId != null) {
			if (dbHelper.checkLoan(mPropertyId)) {
				Loan loan = dbHelper.getLoan(mPropertyId);
				if (loan != null) {
					mPurchasePrice.setText(String.valueOf(loan.getPurchasePrice()));
					mInterestRate.setText(String.valueOf(loan.getInterestRate()));
					mDownPayment.setText(String.valueOf(loan.getDownPayment()));
					mTerm.setText(String.valueOf(loan.getTerm()));
					mHoa.setText(String.valueOf(loan.getHoa()));
					Log.d(TAG, "updateLoanFields(): Update all edit fields");
				} else {
					Log.d(TAG, "updateLoanFields(): Nothing to update");
				}
			}
		} else  {
			Log.d(TAG, "updateLoanFields():  mPropertyId=" + mPropertyId);
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_loan, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void parseInputParameters(){
		Loan loan = new Loan();
		loan.setId(mPropertyId);
		loan.setPurchasePrice(Double.parseDouble(mPurchasePrice.getText().toString()));
		loan.setDownPayment(Double.parseDouble(mDownPayment.getText().toString()));
		loan.setInterestRate(Double.parseDouble(mInterestRate.getText().toString()));
		loan.setTerm(Integer.parseInt(mTerm.getText().toString()));
		loan.setHoa(Double.parseDouble(mHoa.getText().toString()));
		setmLoan(loan);
	}

	public Loan getmLoan() {
		return mLoan;
	}

	public void setmLoan(Loan mLoan) {
		this.mLoan = mLoan;
	}

	
	private boolean checkForEmptyTextBoxes() {
		return 
				mPurchasePrice.getText().toString().isEmpty() ||
				mDownPayment.getText().toString().isEmpty() ||
				mInterestRate.getText().toString().isEmpty() ||
				mTerm.getText().toString().isEmpty() ||
				mHoa.getText().toString().isEmpty();
		
	}
	
	/**
	 * onclick to update loan db
	 * @param view
	 */
	public void updateLoan(View view)
	{
		LoanDatabaseHelper dbHelper = new LoanDatabaseHelper(this);
		
		if (!checkForEmptyTextBoxes()) {
		parseInputParameters();
		
		if (mLoan != null && mPropertyId != null) {
			if (dbHelper.checkLoan(mPropertyId)) {
				dbHelper.updateLoan(mLoan);
				Log.d(TAG, "Loan UPDATED for propertyId=" + mPropertyId);
			} else {
				dbHelper.addLoan(mLoan);
				Log.d(TAG, "Loan UPDATED for propertyId=" + mPropertyId);
			}
			updateLoanFields();
		} else {
			Log.d(TAG, "propertyId=" + mPropertyId);
		}
		
		// After update go back to Manage Loan Activity
		Intent intent = new Intent(this, ManageLoanActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent); }
		else {
			Toast.makeText(getApplicationContext(), "Enter 0 for empty fields.",
					   Toast.LENGTH_SHORT).show();
		}
	}
}
