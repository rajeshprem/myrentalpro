package com.nomoreboundary.myrentalpro.loan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.LoanDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Loan;

public class ManageLoanActivity extends Activity {
	
	private static final String TAG = "ManageLoanActivity";

	private String mPropertyId;
	
	TextView mPurchasePrice, mInterestRate, mDownPayment, mTerm, mHoa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_loan);
		// Show the Up button in the action bar.
		setupActionBar();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		mPurchasePrice = (TextView) findViewById (R.id.txtPurchasePrice);
		mInterestRate = (TextView) findViewById (R.id.txtInterestRate);
		mDownPayment = (TextView) findViewById (R.id.txtDownPayment);
		mTerm = (TextView) findViewById (R.id.txtTerm);
		mHoa = (TextView) findViewById (R.id.txtHoa);
		
		updateLoanFields();
	}

	// Update loan fields if a record already exists
	private void updateLoanFields() {
		if (mPropertyId != null) {
			Loan loan = getLoanParameters(mPropertyId);
			if (loan != null) {
				mPurchasePrice.setText(String.valueOf(loan.getPurchasePrice()));
				mInterestRate.setText(String.valueOf(loan.getInterestRate()));
				mDownPayment.setText(String.valueOf(loan.getDownPayment()));
				mTerm.setText(String.valueOf(loan.getTerm()));
				mHoa.setText(String.valueOf(loan.getHoa()));
				Log.d(TAG, "Update all text fields");
			} else {
				Log.d(TAG, "Nothing to update");
			}
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_loan, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private Loan getLoanParameters(String id){
		LoanDatabaseHelper dbHelper = new LoanDatabaseHelper(this);
		if (dbHelper.checkLoan(id))
			return dbHelper.getLoan(id);
		else 
			return null;
	}
	
	public void openUpdateLoanActivity(View view) {
		Intent intent = new Intent(this, UpdateLoanActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}

}
