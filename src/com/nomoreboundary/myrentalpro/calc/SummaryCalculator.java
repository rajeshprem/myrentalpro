package com.nomoreboundary.myrentalpro.calc;

import android.content.Context;

import com.nomoreboundary.myrentalpro.data.ExpenseDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.RentDatabaseHelper;

public class SummaryCalculator {

	String propertyId;

	Context context;

	String totalRentForCurrentYear, totalExpenseForCurrentYear;

	public SummaryCalculator(Context context, String id) {
		this.context = context;
		this.propertyId = id;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getTotalRentForCurrentYear() {
		RentDatabaseHelper dbHelper = new RentDatabaseHelper(this.context);
		return dbHelper.getTotalRentForCurrentYear(getPropertyId());
	}
	
	public String getTotalRentForAllTime() {
		RentDatabaseHelper dbHelper = new RentDatabaseHelper(this.context);
		return dbHelper.getTotalRentForAllTime(getPropertyId());
	}

	public String getTotalExpenseForCurrentYear() {
		ExpenseDatabaseHelper dbHelper = new ExpenseDatabaseHelper(this.context);
		return dbHelper.getTotalExpenseForCurrentYear(getPropertyId());
	}

	public String getNetIncome() {
		double netIncome = Double.parseDouble(getTotalRentForCurrentYear())
				- Double.parseDouble(getTotalExpenseForCurrentYear());
		return String.valueOf(netIncome);
	}

}
