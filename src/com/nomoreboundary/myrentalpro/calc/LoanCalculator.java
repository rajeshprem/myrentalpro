package com.nomoreboundary.myrentalpro.calc;

import android.content.Context;

import com.nomoreboundary.myrentalpro.data.LoanDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Loan;

public class LoanCalculator {
	Loan mLoan;
	double loanAmount;
	String propertyId;
	LoanDatabaseHelper dbHelper;

	public LoanCalculator(Context context, String id) {
		this.propertyId = id;
		this.dbHelper = new LoanDatabaseHelper(context);
	}

	public Loan getLoan() {
		if (dbHelper.checkLoan(this.propertyId)) {
			return dbHelper.getLoan(this.propertyId);
		} else {
			return dbHelper.getEmptyLoan();
		}
	}

	/**
	 * Initial Loan Amount = Purchase Price - ( ( DownPayment * 100) / Purchase
	 * Price )
	 * 
	 * @return
	 */
	public double getInitialLoanAmount() {
		if (dbHelper.checkLoan(this.propertyId)) {
			Loan loan = dbHelper.getLoan(this.propertyId);
			return loan.getPurchasePrice()
					- (loan.getPurchasePrice() * loan.getDownPayment() / 100.0);
		} else
			return 0.0;
	}

	public double getCurrentLoanAmount() {
		if (dbHelper.checkLoan(this.propertyId)) {
			Loan loan = dbHelper.getLoan(this.propertyId);
			return loan.getPurchasePrice()
					- (loan.getDownPayment() * loan.getPurchasePrice() / 100.0);
		} else
			return 0.0;
	}

	public double getMonthlyPandI() {
		if (dbHelper.checkLoan(this.propertyId)) {
			Loan loan = dbHelper.getLoan(this.propertyId);
			
			double loanAmount = loan.getPurchasePrice()
					- (loan.getDownPayment() * loan.getPurchasePrice() / 100.0);
			
			// Get Monthly Payment
			double payment = MortgageCalculator.calculateMonthlyPayment(
					loanAmount, loan.getTerm(),
					loan.getInterestRate());
			return payment;
		} else
			return 0.0;
	}
}
