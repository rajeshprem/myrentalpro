package com.nomoreboundary.myrentalpro.calc;

import android.content.Context;
import android.util.Log;

import com.nomoreboundary.myrentalpro.data.CashFlowDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.CashFlow;

public class CashFlowCalculator {

	CashFlow mCashFlow;
	double loanAmount;
	String propertyId;
	CashFlowDatabaseHelper dbHelper;
	
	private static final String TAG = "CashFlowCalculator";

	public CashFlowCalculator(Context context, String id) {
		this.propertyId = id;
		this.dbHelper = new CashFlowDatabaseHelper(context);
	}

	public CashFlow getCashFlow() {
		if (dbHelper.checkIfCashFlowParamsExist(this.propertyId)) {
			return dbHelper.getCashFlowParams(this.propertyId);
		} else {
			return dbHelper.getEmptyCashFlowParams();
		}
	}

	// EGI = GPI * VAC %
	public double calcEffectiveGrossIncome() {
		if (dbHelper.checkIfCashFlowParamsExist(this.propertyId)) {
			this.mCashFlow = dbHelper.getCashFlowParams(this.propertyId);
			double egi = mCashFlow.getGrossPotentialIncome()
					- (mCashFlow.getVacancyLoss() * 0.01 * mCashFlow
							.getGrossPotentialIncome());
			Log.d(TAG, "EGI="+ egi);
			mCashFlow.setEffectiveGrossIncome(egi);
			return egi;
		} else {
			return 0.0;
		}
	}

	// GOI = EGI + OI
	public double calcGrossOperatingIncome() {
		if (dbHelper.checkIfCashFlowParamsExist(this.propertyId)) {
			this.mCashFlow = dbHelper.getCashFlowParams(this.propertyId);

			double goi = calcEffectiveGrossIncome() + mCashFlow.getOtherIncome();
			mCashFlow.setGrossOperatingIncome(goi);
			Log.d(TAG, "GOI="+ goi);

			return goi;
		} else {
			return 0.0;
		}
	}

	// NOI = GOI - TOI
	public double calcNetOperatingIncome() {
		if (dbHelper.checkIfCashFlowParamsExist(this.propertyId)) {
			this.mCashFlow = dbHelper.getCashFlowParams(this.propertyId);

			double noi = calcGrossOperatingIncome() - mCashFlow.getTotalOperatingExpenses();
			mCashFlow.setNetOperatingIncome(noi);
			Log.d(TAG, "NOI="+ noi);

			return noi;
		} else {
			return 0.0;
		}
	}

	// BTCF = NOI - RRA - DS
	public double calcBeforeTaxCashFlow() {
		if (dbHelper.checkIfCashFlowParamsExist(this.propertyId)) {
			this.mCashFlow = dbHelper.getCashFlowParams(this.propertyId);

			double btcf = calcNetOperatingIncome()
					- mCashFlow.getReservesReplacementAccount()
					- mCashFlow.getDebtService();
			mCashFlow.setBeforeTaxCashFlow(btcf);
			Log.d(TAG, "BTCF="+ btcf);

			return btcf;
		} else {
			return 0.0;
		}
	}
}
