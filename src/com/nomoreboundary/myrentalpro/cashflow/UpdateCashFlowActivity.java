package com.nomoreboundary.myrentalpro.cashflow;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.CashFlowDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.ExpenseDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.CashFlow;
import com.nomoreboundary.myrentalpro.entity.Property;

public class UpdateCashFlowActivity extends Activity {

	private static final String TAG = "UpdateCashFlowActivity";

	private String mPropertyId;

	EditText mGrossPotentialIncome, mVacancyRate, mOtherIncome, mOperatingExpense, mReservesReplacement, mDebtService;
	
	private CashFlow mCashFlow;


	public CashFlow getmCashFlow() {
		return mCashFlow;
	}

	public void setmCashFlow(CashFlow mCashFlow) {
		this.mCashFlow = mCashFlow;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_cash_flow);
		// Show the Up button in the action bar.
		setupActionBar();
		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		Log.d(TAG, "onCreate() mPropertyId=" + mPropertyId);

		mGrossPotentialIncome = (EditText) findViewById (R.id.edtGrossPotentialIncome);
		mVacancyRate = (EditText) findViewById (R.id.edtVacancyRate);
		mOtherIncome = (EditText) findViewById (R.id.edtOtherIncome);
		mOperatingExpense = (EditText) findViewById (R.id.edtOperatingExpense);
		mReservesReplacement = (EditText) findViewById (R.id.edtReservesReplacement);
		mDebtService = (EditText) findViewById (R.id.edtDebtService);

		updateCashFlowFields();

	}
	
	// Update cashflow fields if a record already exists
		private void updateCashFlowFields() {
			CashFlowDatabaseHelper dbHelper = new CashFlowDatabaseHelper(this);

			if (mPropertyId != null) {
				if (dbHelper.checkIfCashFlowParamsExist(mPropertyId)) {
					CashFlow cf = dbHelper.getCashFlowParams(mPropertyId);
					if (cf != null) {
						mGrossPotentialIncome.setText(String.valueOf(cf.getGrossPotentialIncome()));
						mVacancyRate.setText(String.valueOf(cf.getVacancyLoss()));
						mOtherIncome.setText(String.valueOf(cf.getOtherIncome()));
						mOperatingExpense.setText(String.valueOf(cf.getTotalOperatingExpenses()));
						mReservesReplacement.setText(String.valueOf(cf.getReservesReplacementAccount()));
						mDebtService.setText(String.valueOf(cf.getDebtService()));
						Log.d(TAG, "updateCashFlowFields(): Update all edit fields");
					} else {
						// Get Current Rent if set for property
						PropertyDatabaseHelper pdbHelper = new PropertyDatabaseHelper(this);
						Property property = pdbHelper.getProperty(mPropertyId);
						Log.d(TAG, "rent=" + property.getRent());
						if (property != null) {
							mGrossPotentialIncome.setText(String.valueOf(property.getRent() * 12));
						} 
						
						// Get Expenses from DB for OE
						ExpenseDatabaseHelper expDbHelper = new ExpenseDatabaseHelper(this);
						String expense = expDbHelper.getTotalExpenseForCurrentYear(mPropertyId);
						mOperatingExpense.setText(expense);
						
						// Get Loan from DB for DS
						// TODO
					}
				}
			} else  {
				Log.e(TAG, "updateCashFlowFields():  mPropertyId=" + mPropertyId);
			}
		}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_cash_flow, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private boolean checkForEmptyTextBoxes() {
		return 
		mGrossPotentialIncome.getText().toString().isEmpty() ||
		mVacancyRate.getText().toString().isEmpty() ||
		mOtherIncome.getText().toString().isEmpty() ||
		mOperatingExpense.getText().toString().isEmpty() ||
		mReservesReplacement.getText().toString().isEmpty() ||
		mDebtService.getText().toString().isEmpty();
		
	}
	private void parseInputParameters(){
		CashFlow cf = new CashFlow();
		cf.setId(mPropertyId);
		cf.setGrossPotentialIncome(Double.parseDouble(mGrossPotentialIncome.getText().toString()));
		cf.setVacancyLoss(Double.parseDouble(mVacancyRate.getText().toString()));
		cf.setOtherIncome(Double.parseDouble(mOtherIncome.getText().toString()));
		cf.setTotalOperatingExpenses(Double.parseDouble(mOperatingExpense.getText().toString()));
		cf.setReservesReplacementAccount(Double.parseDouble(mReservesReplacement.getText().toString()));
		cf.setDebtService(Double.parseDouble(mDebtService.getText().toString()));
		setmCashFlow(cf);
	}

	/**
	 * on click for update cashflow
	 * @param view
	 */
	public void openUpdateCashFlowActivity(View view) {

		CashFlowDatabaseHelper dbHelper = new CashFlowDatabaseHelper(this);

		if (!checkForEmptyTextBoxes()) {
			parseInputParameters(); 
		

		if (mCashFlow != null && mPropertyId != null) {
			if (dbHelper.checkIfCashFlowParamsExist(mPropertyId)) {
				dbHelper.updateCashFlowParams(mCashFlow);
				Log.d(TAG, "CF UPDATED for propertyId=" + mPropertyId);
			} else {
				dbHelper.addCashFlowParams(mCashFlow);
				Log.d(TAG, "CF Added for propertyId=" + mPropertyId);
			}
			updateCashFlowFields();
		} else {
			Log.d(TAG, "propertyId=" + mPropertyId);
		}

		Intent intent = new Intent(this, ManageCashFlowActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
		}  else {
			Toast.makeText(getApplicationContext(), "Enter 0 for empty fields.",
					   Toast.LENGTH_SHORT).show();
		}
	}
	
	// help dialogs
	
		public void openGPIHelp(View view) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
			alertDialog.setTitle("Gross Potential Income");
			alertDialog.setMessage("Sum of all potential income on your property. For ex: Monthly Rent $900 x 12 = $18000");
			alertDialog.show();
		}
		
		public void openVACHelp(View view) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
			alertDialog.setTitle("Vacancy Rate (%)");
			alertDialog.setMessage("% of time units are vacant on average.  Avg is 5%");
			alertDialog.show();
		}

		public void openOIHelp(View view) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
			alertDialog.setTitle("Other Income (Year)");
			alertDialog.setMessage("Money Received from sources other than Rent. Ex: parking fees.");
			alertDialog.show();
		}

		public void openOEHelp(View view) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
			alertDialog.setTitle("Operating Expenses (Year)");
			alertDialog.setMessage("Money spent on operations.  Ex: generators, salaries");
			alertDialog.show();
		}

		public void openRRAHelp(View view) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
			alertDialog.setTitle("Reserves Replacement Account");
			alertDialog.setMessage("Costs of items that wear out. For ex: Roofing, exterior paint");
			alertDialog.show();
		}

		public void openDSHelp(View view) {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
			alertDialog.setTitle("Debt Service");
			alertDialog.setMessage("This is your mortgage payment.");
			alertDialog.show();
		}
}
