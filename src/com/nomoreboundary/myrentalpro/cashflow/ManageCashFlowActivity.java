package com.nomoreboundary.myrentalpro.cashflow;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.CashFlowDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.ExpenseDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.LoanDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.CashFlow;
import com.nomoreboundary.myrentalpro.entity.Property;

public class ManageCashFlowActivity extends Activity {

	private static final String TAG = "ManageCashFlowActivity";

	private String mPropertyId;
	
	TextView mGrossPotentialIncome, mVacancyRate, mOtherIncome, mOperatingExpense, mReservesReplacement, mDebtService;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_cash_flow);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		mGrossPotentialIncome = (TextView) findViewById (R.id.txtGrossPotentialIncome);
		mVacancyRate = (TextView) findViewById (R.id.txtVacancyRate);
		mOtherIncome = (TextView) findViewById (R.id.txtOtherIncome);
		mOperatingExpense = (TextView) findViewById (R.id.txtOperatingExpense);
		mReservesReplacement = (TextView) findViewById (R.id.txtReservesReplacement);
		mDebtService = (TextView) findViewById (R.id.txtDebtService);

		updateCashFlowFields();

	}

	// Update loan fields if a record already exists
	private void updateCashFlowFields() {
		if (mPropertyId != null) {
			CashFlow cf = getCashFlowParameters(mPropertyId);
			if (cf != null) {
				mGrossPotentialIncome.setText(String.valueOf(cf
						.getGrossPotentialIncome()));
				mVacancyRate.setText(String.valueOf(cf.getVacancyLoss()));
				mOtherIncome.setText(String.valueOf(cf.getOtherIncome()));
				mOperatingExpense.setText(String.valueOf(cf
						.getTotalOperatingExpenses()));
				mReservesReplacement.setText(String.valueOf(cf
						.getReservesReplacementAccount()));
				mDebtService.setText(String.valueOf(cf.getDebtService()));
				Log.d(TAG, "Update all text fields");
			} else {
				// Get Current Rent if set for property
				PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
				Property property = dbHelper.getProperty(mPropertyId);
				Log.d(TAG, "rent=" + property.getRent());
				if (property != null) {
					mGrossPotentialIncome.setText(String.valueOf(property.getRent() * 12));
				} 
				
				// Get Expenses from DB for OE
				ExpenseDatabaseHelper expDbHelper = new ExpenseDatabaseHelper(this);
				String expense = expDbHelper.getTotalExpenseForCurrentYear(mPropertyId);
				mOperatingExpense.setText(expense);
				
				// Get Loan from DB for DS
				// TODO
				
			}
		}
	}
		
	private CashFlow getCashFlowParameters(String id) {
		CashFlowDatabaseHelper dbHelper = new CashFlowDatabaseHelper(this);
		if (dbHelper.checkIfCashFlowParamsExist(id))
			return dbHelper.getCashFlowParams(id);
		else
			return null;
	}
	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_cash_flow, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void openEditCashFlowActivity(View view) {
		Intent intent = new Intent(this, UpdateCashFlowActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}
	
	// help dialogs
	
	public void openGPIHelp(View view) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
		alertDialog.setTitle("Gross Potential Income");
		alertDialog.setMessage("Sum of all potential income on your property. For ex: Monthly Rent $900 x 12 = $18000");
		alertDialog.show();
	}
	
	public void openVACHelp(View view) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
		alertDialog.setTitle("Vacancy Rate (%)");
		alertDialog.setMessage("% of time units are vacant on average.  Avg is 5%");
		alertDialog.show();
	}

	public void openOIHelp(View view) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
		alertDialog.setTitle("Other Income (Year)");
		alertDialog.setMessage("Money Received from sources other than Rent. Ex: parking fees.");
		alertDialog.show();
	}

	public void openOEHelp(View view) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
		alertDialog.setTitle("Operating Expenses (Year)");
		alertDialog.setMessage("Money spent on operations.  Ex: generators, salaries");
		alertDialog.show();
	}

	public void openRRAHelp(View view) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
		alertDialog.setTitle("Reserves Replacement Account");
		alertDialog.setMessage("Costs of items that wear out. For ex: Roofing, exterior paint");
		alertDialog.show();
	}

	public void openDSHelp(View view) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); 
		alertDialog.setTitle("Debt Service");
		alertDialog.setMessage("This is your mortgage payment.");
		alertDialog.show();
	}
	

}
