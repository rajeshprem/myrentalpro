package com.nomoreboundary.myrentalpro.tenant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailActivity;
import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;

public class EditTenantActivity extends Activity {

	private static final String TAG = "EditTenantActivity";
	private String mPropertyId;
	
	EditText mTenantName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_tenant);
		// Show the Up button in the action bar.
		setupActionBar();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		mTenantName = (EditText) findViewById(R.id.edtTenantName);

		
		
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_tenant, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * onclick listener for tenant update info
	 * @param view
	 */
	public void updateTenantInfo(View view) {
		
		String sTenantName = mTenantName.getText().toString();
		if (TextUtils.isEmpty(sTenantName)) {
		    Toast.makeText(this, "Please enter a name.", Toast.LENGTH_SHORT).show();
		    return;
		}
		
		// Update Tenant info
		PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
		int i = dbHelper.updateTenant(mPropertyId, sTenantName);
		
		// Log
		if (i == 0) {
		    Toast.makeText(this, "No properties updated. ID=" + mPropertyId, Toast.LENGTH_SHORT).show();
		} else {
			Log.i(TAG, "Tenant=" + sTenantName+ " updated for Property ID=" + mPropertyId);
		}

		// After update go back to Property Detail Activity
		Intent intent = new Intent(this, PropertyDetailActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}
}
