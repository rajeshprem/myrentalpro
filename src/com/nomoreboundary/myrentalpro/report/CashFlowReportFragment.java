package com.nomoreboundary.myrentalpro.report;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.calc.CashFlowCalculator;
import com.nomoreboundary.myrentalpro.entity.CashFlow;

public class CashFlowReportFragment extends Fragment {
	private static final String TAG = "CashFlowFragment";
	public static final String ARG_PROPERTY_ID = "property_id";
	public static final String cur = MyRentalProApplication.getCurrencySymbol() + " ";

	String mPropertyId;
	
	TextView mGrossPotentialIncome;
	TextView mVacancyLoss;
	TextView mOtherIncome;
	TextView mEffectiveGrossIncome;
	TextView mGrossOperatingIncome;
	TextView mTotalOperatingExpenses;
	TextView mNetOperatingIncome;
	TextView mReservesReplacementAccount;
	TextView mDebtService;
	TextView mBeforeTaxCashFlow;
	
	public CashFlowReportFragment() {
		
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences prefs = getActivity().getSharedPreferences(
			      "com.nomoreboundary.myrental.pro", Context.MODE_PRIVATE);
		mPropertyId = prefs.getString(ARG_PROPERTY_ID, "0");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_property_report_cashflow, container, false);
		Log.d(TAG, ARG_PROPERTY_ID + "=" + mPropertyId);

		 mGrossPotentialIncome = (TextView) rootView.findViewById(R.id.txtCashFlowReportGrossPotentialIncome);
		 //mVacancyLoss = (TextView) rootView.findViewById(R.id.txtCashFlowReportVacancyLoss);
		 //mOtherIncome = (TextView) rootView.findViewById(R.id.txtCashFlowReportOtherIncome);
		 mEffectiveGrossIncome = (TextView) rootView.findViewById(R.id.txtCashFlowReportEffectiveGrossIncome);
		 mGrossOperatingIncome = (TextView) rootView.findViewById(R.id.txtCashFlowReportGrossOperatingIncome);
		 mTotalOperatingExpenses = (TextView) rootView.findViewById(R.id.txtCashFlowReportTotalOperatingExpenses);
		 mNetOperatingIncome = (TextView) rootView.findViewById(R.id.txtCashFlowReportNetOperatingIncome);
		 mReservesReplacementAccount = (TextView) rootView.findViewById(R.id.txtCashFlowReportReservesReplacement);
		 mDebtService = (TextView) rootView.findViewById(R.id.txtCashFlowReportDebtService);
		 mBeforeTaxCashFlow  = (TextView) rootView.findViewById(R.id.txtCashFlowReportBeforeTaxCashFlow);
	   	populateCashFlowParams(mPropertyId);
		
		return rootView;
	}
	
	/**
	 * Set all text views in report
	 * @param id
	 */
	private void populateCashFlowParams(String id)
	{
		CashFlowCalculator c = new CashFlowCalculator(getActivity(), id);
		CashFlow cf = c.getCashFlow();
		if (cf != null) {
		mGrossPotentialIncome.setText(String.valueOf((int)cf.getGrossPotentialIncome()));
		//mVacancyLoss.setText(String.valueOf(cf.getVacancyLoss()));
		//mOtherIncome.setText(String.valueOf(cf.getOtherIncome()));
		mEffectiveGrossIncome.setText(String.valueOf((int)c.calcEffectiveGrossIncome()));
		mGrossOperatingIncome.setText(String.valueOf((int)c.calcGrossOperatingIncome()));
		mTotalOperatingExpenses.setText(String.valueOf((int)cf.getTotalOperatingExpenses()));
		mNetOperatingIncome.setText(String.valueOf((int)c.calcNetOperatingIncome()));
		mReservesReplacementAccount.setText(String.valueOf((int)cf.getReservesReplacementAccount()));
		mDebtService.setText(String.valueOf((int)cf.getDebtService()));
		mBeforeTaxCashFlow.setText(String.valueOf((int)c.calcBeforeTaxCashFlow()));  
		} else {
			Log.e(TAG, "cashflow obj is null");
		}
		Log.d(TAG, "Populated CashFlow params");
	}
}
