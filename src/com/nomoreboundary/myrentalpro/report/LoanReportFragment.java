package com.nomoreboundary.myrentalpro.report;

import java.text.DecimalFormat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.calc.LoanCalculator;

public class LoanReportFragment extends Fragment {
	
	private static final String TAG = "LoanReportFragment";
	public static final String ARG_PROPERTY_ID = "property_id";
	public static final String cur = MyRentalProApplication.getCurrencySymbol() + " ";

	String mPropertyId;
	TextView mPurchasePrice, mInitialLoanAmount, mCurrentLoanAmount, mInterestRate, mMonthlyPandI, mHOA ;

	public LoanReportFragment() {
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences prefs = getActivity().getSharedPreferences(
			      "com.nomoreboundary.myrental.pro", Context.MODE_PRIVATE);
		mPropertyId = prefs.getString(ARG_PROPERTY_ID, "0");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_property_report_loan, container, false);
		Log.d(TAG, ARG_PROPERTY_ID + "=" + mPropertyId);

		mPurchasePrice = (TextView) rootView.findViewById(R.id.txtReportLoanPurchasePrice);
		mInitialLoanAmount = (TextView) rootView.findViewById(R.id.txtReportLoanInitialAmount);
		mCurrentLoanAmount = (TextView) rootView.findViewById(R.id.txtReportLoanCurrentAmount);
		mInterestRate = (TextView) rootView.findViewById(R.id.txtReportLoanInterestRate);
		mMonthlyPandI = (TextView) rootView.findViewById(R.id.txtReportLoanMonthPandI);
		mHOA = (TextView) rootView.findViewById(R.id.txtReportLoanHOA);
		
		populateLoanParams(mPropertyId);
		
		return rootView;
	}
	
	/**
	 * Set all text views in report
	 * @param id
	 */
	private void populateLoanParams(String id)
	{
		LoanCalculator l = new LoanCalculator(getActivity(), id);
		mPurchasePrice.setText(String.valueOf((int)l.getLoan().getPurchasePrice()));
		mInitialLoanAmount.setText(String.valueOf((int)l.getInitialLoanAmount()));
		mCurrentLoanAmount.setText(String.valueOf((int)l.getCurrentLoanAmount()));
		mInterestRate.setText(String.valueOf(l.getLoan().getInterestRate()));
		DecimalFormat df = new DecimalFormat("#.##");
		mMonthlyPandI.setText(String.valueOf(df.format(l.getMonthlyPandI())));
		mHOA.setText(String.valueOf((int)l.getLoan().getHoa()));

	}

}
