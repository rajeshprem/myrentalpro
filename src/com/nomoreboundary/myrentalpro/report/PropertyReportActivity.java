package com.nomoreboundary.myrentalpro.report;

import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;

public class PropertyReportActivity extends FragmentActivity {
	
	private static final String TAG = "PropertyReportActivity";
	
	public static final String ARG_PROPERTY_ID = "property_id";

	String mPropertyId;
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_property_report);
		
		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.property_report, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			
			// Add this to intent to provide the correct property name to navigate to parent task
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

	        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
	            // This activity is NOT part of this app's task, so create a new task
	            // when navigating up, with a synthesized back stack.
	            TaskStackBuilder.create(this)
	                    // Add all of this activity's parents to the back stack
	                    .addNextIntentWithParentStack(upIntent)
	                    // Navigate up to the closest parent
	                    .startActivities();
	        } else {
	            // This activity is part of this app's task, so simply
	            // navigate up to the logical parent activity.
	            NavUtils.navigateUpTo(this, upIntent);
	        }
	        return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			
			switch (position) {
	        case 0:
	            // Rental Report fragment activity
	        	return new RentReportFragment();
	        case 1:
	            // Expense Report fragment activity
	        	return new ExpenseReportFragment();
	        case 2:
	            // Loan Report fragment activity
	        	return new LoanReportFragment();
	        case 3:
	            // Summary Report fragment activity
	        	return new CashFlowReportFragment();
	        case 4:
	            // Summary Report fragment activity
	        	return new SummaryReportFragment();
	        }
			return null;
		}

		@Override
		public int getCount() {
			// Show 5 total pages.
			return 5;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_rental).toUpperCase(l);
			case 1:
				return getString(R.string.title_expenses).toUpperCase(l);
			case 2:
				return getString(R.string.title_loan).toUpperCase(l);
			case 3:
				return getString(R.string.title_cashflow).toUpperCase(l);
			case 4:
				return getString(R.string.title_summary).toUpperCase(l);
			}
			return null;
		}
	}
}
