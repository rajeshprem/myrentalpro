package com.nomoreboundary.myrentalpro.report;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.RentDatabaseHelper;

public class RentReportFragment extends Fragment {

	TextView txtTotalRentAllTime, txtTotalRentCurrentYear;

	String mPropertyId;

	public static final String ARG_PROPERTY_ID = "property_id";

	private static final String TAG = "RentReportFragment";

	String totalRent;

	public RentReportFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences prefs = getActivity().getSharedPreferences(
				"com.nomoreboundary.myrental.pro", Context.MODE_PRIVATE);
		mPropertyId = prefs.getString(ARG_PROPERTY_ID, "0");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_property_report_rental, container, false);

		Log.d(TAG, ARG_PROPERTY_ID + "=" + mPropertyId);

		RentDatabaseHelper dbHelper = new RentDatabaseHelper(getActivity());

		txtTotalRentCurrentYear = (TextView) rootView.findViewById(R.id.txtImgRentalIncome);
		txtTotalRentAllTime = (TextView) rootView.findViewById(R.id.txtImgRentalIncomeOverall);
		
		txtTotalRentCurrentYear.setText(dbHelper.getTotalRentForCurrentYear(mPropertyId));
		txtTotalRentAllTime.setText(dbHelper.getTotalRentForAllTime(mPropertyId));

		
		return rootView;
	}


}
