package com.nomoreboundary.myrentalpro.report;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.calc.SummaryCalculator;

public class SummaryReportFragment extends Fragment {

	TextView txtOverallCurrentYear;

	String mPropertyId;

	public static final String ARG_PROPERTY_ID = "property_id";

	private static final String TAG = "SummaryReportFragment";

	String netIncome;

	public SummaryReportFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences prefs = getActivity().getSharedPreferences(
				"com.nomoreboundary.myrental.pro", Context.MODE_PRIVATE);
		mPropertyId = prefs.getString(ARG_PROPERTY_ID, "0");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(
				R.layout.fragment_property_report_summary, container, false);
	
		
			Log.d(TAG, ARG_PROPERTY_ID + "=" + mPropertyId);
			netIncome = getNetIncome(mPropertyId);
		
			txtOverallCurrentYear = (TextView) rootView
					.findViewById(R.id.txtSummaryOverallCurrentYear);
			txtOverallCurrentYear.setText(netIncome);
		
		return rootView;
	}

	private String getNetIncome(String id)
	{
		SummaryCalculator s = new SummaryCalculator(getActivity(), id);
		return s.getNetIncome();
		
	}
}
