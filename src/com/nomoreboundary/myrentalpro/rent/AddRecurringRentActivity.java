package com.nomoreboundary.myrentalpro.rent;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.data.RentDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Property;
import com.nomoreboundary.myrentalpro.util.MoneyValueFilter;

public class AddRecurringRentActivity extends FragmentActivity {

	private static final String TAG = "AddRecurringRentActivity";
	public static final String ARG_PROPERTY_ID = "property_id";

	EditText mAmount;
	static String mFromDate, mToDate;

	public String mPropertyId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_recurring_rent);
		// Show the Up button in the action bar.
		setupActionBar();

		mAmount = (EditText) findViewById(R.id.edtRecurringRentAmount);
		mAmount.setFilters(new InputFilter[] { new MoneyValueFilter() }); 

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_recurring_rent, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void showFromDatePickerDialog(View v) {
		DialogFragment newFragment = new FromDatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}
	
	public void showToDatePickerDialog(View v) {
		DialogFragment newFragment = new ToDatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	public static void setFromDate(String formattedDate) {
		mFromDate = formattedDate;
	}

	public static void setToDate(String formattedDate) {
		mToDate = formattedDate;
	}

	/**
	 * on click listener for submitting recurring rent button
	 * 
	 * @param view
	 */
	public void submitRecurringRent(View view) {
		Log.d(TAG, "Attempting to add recurring rent to database");

		// Get Rent
		String rent = mAmount.getText().toString();
		if (rent.isEmpty()) {
			Toast.makeText(this, "Please enter an amount.", Toast.LENGTH_SHORT)
					.show();
			return;
		}

		Double amount = Double.parseDouble(rent);
		Log.d(TAG, "Amount=" + amount);

		Log.d(TAG, "From Date=" + mFromDate);
		Log.d(TAG, "To Date=" + mToDate);

		// Get Property
		Log.d(TAG, "PropertyId=" + mPropertyId);
		PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
		Property property = dbHelper.getProperty(mPropertyId);

		// Add to Rent Database
		RentDatabaseHelper rentDbHelper = new RentDatabaseHelper(this);
		rentDbHelper.addRent(property, amount, mFromDate, mToDate);

		Log.d(TAG, "Added Recurring Rent to DB");

		// Go back to manage rent page
		Intent intent = new Intent(this, ManageRentActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}

	public static class FromDatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
			String formattedDate = String.valueOf(year) + "-"
					+ String.valueOf(month + 1) + "-" + String.valueOf(day);
			Log.d(TAG, "Formatted FROM Date picked=" + formattedDate);
			setFromDate(formattedDate);
		}
	}

	public static class ToDatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
			String formattedDate = String.valueOf(year) + "-"
					+ String.valueOf(month + 1) + "-" + String.valueOf(day);
			Log.d(TAG, "Formatted TO Date picked=" + formattedDate);
			setToDate(formattedDate);
		}
	}

}
