package com.nomoreboundary.myrentalpro.rent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.RentDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Rent;

public class ManageRentActivity extends FragmentActivity implements
DeleteItemDialogFragment.DeleteItemDialogListener {

	private static final String TAG = "ManageRentActivity";
	public String mPropertyId;
	
	ListView rentListView;
	
	ArrayList<HashMap<String, String>> rentsList = new ArrayList<HashMap<String, String>>();

	static final String KEY_TENANT = "tenant";
	static final String KEY_DATE = "date";
	static final String KEY_AMOUNT = "amount";
	
	ListRentsAdapter complexAdapter;
	
	private Rent mRent;
	private int mPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_rent);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		rentListView = (ListView) findViewById(R.id.listRents);
		
		// Initalize the Rent database
		RentDatabaseHelper dbHelper = new RentDatabaseHelper(getApplicationContext());
		List<Rent> rents = dbHelper.getAllRentsForProperty(mPropertyId);
			
		// Populate list
		rentsList.clear();
		for (Rent rent : rents)
		{
			HashMap<String, String> listItem = new HashMap<String, String>();
			listItem.put(KEY_AMOUNT, String.valueOf(rent.getAmount()));
			listItem.put(KEY_DATE, rent.getDate());
			listItem.put(KEY_TENANT, rent.getTenant());
			rentsList.add(listItem);
		}
		
		// Set up adapters
		complexAdapter = new ListRentsAdapter(this, rentsList);
		rentListView.setAdapter(complexAdapter);
		
		// Set up a long click listener on a view item
		rentListView.setLongClickable(true);
		rentListView.setOnItemLongClickListener(new OnItemLongClickListener() {
		    public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
		    	
		    	// Put current list item in a similar hashmap
		    	TextView tenant = (TextView) v.findViewById(R.id.tenant_name);
		    	TextView date = (TextView) v.findViewById(R.id.date);
		    	TextView amount = (TextView) v.findViewById(R.id.amount);
		    	Log.d(TAG, tenant.getText() + ":" + date.getText() + ":" + amount.getText());
		    	
		    	// Create the rent object
		    	double dblAmount = Double.parseDouble(amount.getText().toString());
		    	mRent = new Rent(mPropertyId, tenant.getText().toString(), date.getText().toString(), dblAmount);
		    	mPosition = position;
		    	
		        //Try to Remove using dialog box
				showRemoveDialog();
				
				// Remove item from listview and repopulate list
		        return true;
		    }
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_rent, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			
			// Add this to intent to provide the correct property name to navigate to parent task
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

	        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
	            // This activity is NOT part of this app's task, so create a new task
	            // when navigating up, with a synthesized back stack.
	            TaskStackBuilder.create(this)
	                    // Add all of this activity's parents to the back stack
	                    .addNextIntentWithParentStack(upIntent)
	                    // Navigate up to the closest parent
	                    .startActivities();
	        } else {
	            // This activity is part of this app's task, so simply
	            // navigate up to the logical parent activity.
	            NavUtils.navigateUpTo(this, upIntent);
	        }
	        return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * on click listener for add rent activity
	 * @param view
	 */
	public void openAddRentActivity(View view)
	{
		Intent intent = new Intent(this, AddRentActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}
	
	public void openAddRecurringRentActivity(View view)
	{
		Intent intent = new Intent(this, AddRecurringRentActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}
	
	/**
	 * Show the Remove Item Dialog Fragment
	 */
	private void showRemoveDialog() {
		DialogFragment removeDialogFragment = new DeleteItemDialogFragment();
		removeDialogFragment.show(getSupportFragmentManager(),
				"DeleteItemDialogFragment");
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		RentDatabaseHelper dbHelper = new RentDatabaseHelper(this);
		dbHelper.deleteRent(mRent);
		rentsList.remove(mPosition); 
		complexAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// Cancel and Return to Activity
		
	}
	
	

}
