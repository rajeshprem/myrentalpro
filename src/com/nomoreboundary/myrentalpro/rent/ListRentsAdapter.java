package com.nomoreboundary.myrentalpro.rent;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.R;

public class ListRentsAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater = null;

	public ListRentsAdapter(Activity a,
			ArrayList<HashMap<String, String>> d) {
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.list_rent_row, null);

		TextView tenant = (TextView) vi.findViewById(R.id.tenant_name);
		TextView date = (TextView) vi.findViewById(R.id.date);
		TextView amount = (TextView) vi.findViewById(R.id.amount);

		HashMap<String, String> rentMap = new HashMap<String, String>();
		rentMap = data.get(position);

		// Setting all values in listview
		tenant.setText(rentMap.get(ManageRentActivity.KEY_TENANT));
		date.setText(rentMap.get(ManageRentActivity.KEY_DATE));
		amount.setText(rentMap.get(ManageRentActivity.KEY_AMOUNT));

		return vi;
	}
}
