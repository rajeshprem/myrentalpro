package com.nomoreboundary.myrentalpro.expenses;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.R;

public class ListExpensesAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private static LayoutInflater inflater = null;

	public ListExpensesAdapter(Activity a,
			ArrayList<HashMap<String, String>> d) {
		activity = a;
		data = d;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.list_expense_row, null);

		TextView type = (TextView) vi.findViewById(R.id.txtExpenseType);
		TextView description = (TextView) vi.findViewById(R.id.txtExpenseDescription);
		TextView date = (TextView) vi.findViewById(R.id.txtExpenseDate);
		TextView amount = (TextView) vi.findViewById(R.id.txtExpenseAmount);

		HashMap<String, String> expenseMap = new HashMap<String, String>();
		expenseMap = data.get(position);

		// Setting all values in listview
		type.setText(expenseMap.get(ManageExpenseActivity.KEY_TYPE));
		description.setText(expenseMap.get(ManageExpenseActivity.KEY_DESCRIPTION));
		date.setText(expenseMap.get(ManageExpenseActivity.KEY_DATE));
		amount.setText(expenseMap.get(ManageExpenseActivity.KEY_AMOUNT));

		return vi;
	}
}
