package com.nomoreboundary.myrentalpro.expenses;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.ExpenseDatabaseHelper;

public class AddExpenseActivity extends FragmentActivity implements OnItemSelectedListener{

	private static final String TAG = "AddExpenseActivity";
	
	private String mPropertyId;
	
	EditText mExpenseAmount, mExpenseDescription;
	Spinner mExpenseType;
	
	String mType;
	static String mDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_expense);
		// Show the Up button in the action bar.
		setupActionBar();
		
		mExpenseAmount = (EditText) findViewById (R.id.edtExpenseAmount);
		mExpenseDescription = (EditText) findViewById (R.id.edtDescription);
		initSpinner();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}

	}
	
	private void initSpinner()
	{
		mExpenseType = (Spinner) findViewById (R.id.spnExpenseType);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.expense_types, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		mExpenseType.setAdapter(adapter);
		mExpenseType.setOnItemSelectedListener(this);

	}
	
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// An item was selected. You can retrieve the selected item using
		mType = (String) parent.getItemAtPosition(pos);
	}

	public void onNothingSelected(AdapterView<?> parent) {
		// Another interface callback
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_expense, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void showDatePickerDialog(View v) {
	    DialogFragment newFragment = new DatePickerFragment();
	    newFragment.show(getSupportFragmentManager(), "datePicker");
	}
	
	public static void setDate(String formattedDate)
	{
		mDate = formattedDate;
	}
	
	public static class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
			String formattedDate =  String.valueOf(year) + "-" 
									+ String.valueOf(month + 1) + "-"
									+ String.valueOf(day);
			Log.d(TAG, "Formatted Date picked=" + formattedDate);
			setDate(formattedDate);
		}
	}
	/**
	 * on click listener for rent
	 * @param view
	 */
	public void submitExpense(View view)
	{
		Log.d(TAG, "Adding to Expense database");
		
		// Get Rent
		String eAmount = mExpenseAmount.getText().toString();
		if (eAmount.isEmpty()) {
			Toast.makeText(this, "Please enter an amount.", Toast.LENGTH_SHORT).show();
		    return;
		}
		
		Double amount = Double.parseDouble(eAmount);
		Log.d(TAG, "Amount=" + amount );

		// Add to Expense Database
		ExpenseDatabaseHelper expenseDbHelper = new ExpenseDatabaseHelper(this);
		expenseDbHelper.addExpense(mPropertyId, mType, mExpenseDescription.getText().toString(), amount, mDate);
		
		Log.d(TAG, "Added to Expense DB");

		// Go back to manage rent page
		Intent intent = new Intent(this, ManageExpenseActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}

}
