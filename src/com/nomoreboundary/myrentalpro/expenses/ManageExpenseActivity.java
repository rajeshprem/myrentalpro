package com.nomoreboundary.myrentalpro.expenses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.ExpenseDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Expense;
import com.nomoreboundary.myrentalpro.rent.AddRentActivity;
import com.nomoreboundary.myrentalpro.rent.DeleteItemDialogFragment;

public class ManageExpenseActivity extends FragmentActivity implements
DeleteItemDialogFragment.DeleteItemDialogListener  {
	
	private static final String TAG = "ManageExpenseActivity";
	public String mPropertyId;
	
	ListView expenseListView;
	ListExpensesAdapter complexAdapter;
	ArrayList<HashMap<String, String>> expensesList = new ArrayList<HashMap<String, String>>();


	static final String KEY_TYPE = "type"; 
	static final String KEY_DESCRIPTION = "description"; 
	static final String KEY_DATE = "date";
	static final String KEY_AMOUNT = "amount";

	private Expense mExpense;
	private int mPosition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_expense);
		// Show the Up button in the action bar.
		setupActionBar();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		expenseListView = (ListView) findViewById(R.id.listExpense);
		
		// Initalize the Expense database
		ExpenseDatabaseHelper dbHelper = new ExpenseDatabaseHelper(getApplicationContext());
		List<Expense> expenses = dbHelper.getAllExpensesForProperty(mPropertyId);
	
		// Populate list
				expensesList.clear();
				for (Expense expense : expenses)
				{
					HashMap<String, String> listItem = new HashMap<String, String>();
					listItem.put(KEY_AMOUNT, String.valueOf(expense.getAmount()));
					listItem.put(KEY_DATE, expense.getDate());
					listItem.put(KEY_TYPE, expense.getType());
					Log.d(TAG, KEY_TYPE+"="+expense.getType());
					listItem.put(KEY_DESCRIPTION, expense.getDescription());
					expensesList.add(listItem);
				}

		// Set up adapters
		complexAdapter = new ListExpensesAdapter(this, expensesList);
		expenseListView.setAdapter(complexAdapter);
		
		// Set up a long click listener on a view item
		// Set up a long click listener on a view item
		expenseListView.setLongClickable(true);
		expenseListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
				    	
						// Put current list item in a similar hashmap
				    	TextView type = (TextView) v.findViewById(R.id.txtExpenseType);
				    	TextView date = (TextView) v.findViewById(R.id.txtExpenseDate);
				    	TextView amount = (TextView) v.findViewById(R.id.txtExpenseAmount);
				    	TextView description = (TextView) v.findViewById(R.id.txtExpenseDescription);
				    	Log.d(TAG, type.getText() + ":" + date.getText() + ":" + amount.getText());
				    	
				    	// Create the rent object
				    	double dblAmount = Double.parseDouble(amount.getText().toString());
				    	mExpense = new Expense(mPropertyId, type.getText().toString(), description.getText().toString(), date.getText().toString(), dblAmount);
				    	mPosition = position;
				    	
				        //Try to Remove using dialog box
						showRemoveDialog();
						
						// Remove item from listview and repopulate list
				        return true;
				    }
				});
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_expense, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			
			// Add this to intent to provide the correct property name to navigate to parent task
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

	        if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
	            // This activity is NOT part of this app's task, so create a new task
	            // when navigating up, with a synthesized back stack.
	            TaskStackBuilder.create(this)
	                    // Add all of this activity's parents to the back stack
	                    .addNextIntentWithParentStack(upIntent)
	                    // Navigate up to the closest parent
	                    .startActivities();
	        } else {
	            // This activity is part of this app's task, so simply
	            // navigate up to the logical parent activity.
	            NavUtils.navigateUpTo(this, upIntent);
	        }
	        return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Show the Remove Item Dialog Fragment
	 */
	private void showRemoveDialog() {
		DialogFragment removeDialogFragment = new DeleteItemDialogFragment();
		removeDialogFragment.show(getSupportFragmentManager(),
				"DeleteItemDialogFragment");
	}
	
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		ExpenseDatabaseHelper dbHelper = new ExpenseDatabaseHelper(this);
		dbHelper.deleteExpense(mExpense);
		expensesList.remove(mPosition); 
		complexAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// Cancel and Return to Activity
		
	}
	
	public void openAddExpense(View view)
	{
		Intent intent = new Intent(this, AddExpenseActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}

}
