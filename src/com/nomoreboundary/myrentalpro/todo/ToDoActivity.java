package com.nomoreboundary.myrentalpro.todo;

import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class ToDoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_to_do);
	}
}
