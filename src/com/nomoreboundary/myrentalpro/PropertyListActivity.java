package com.nomoreboundary.myrentalpro;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Property;
import com.nomoreboundary.myrentalpro.property.AddPropertyActivity;
import com.nomoreboundary.myrentalpro.report.PropertyReportActivity;

/**
 * An activity representing a list of Properties. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link PropertyDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link PropertyListFragment} and the item details (if present) is a
 * {@link PropertyDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link PropertyListFragment.Callbacks} interface to listen for item
 * selections.
 */
public class PropertyListActivity extends FragmentActivity implements
		PropertyListFragment.Callbacks {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;
	
	List<Property> propertyList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
		propertyList = dbHelper.getAllProperties();
		
		if (propertyList.isEmpty()) {
		setContentView(R.layout.activity_property_list_empty);
		}
		else  {
		setContentView(R.layout.activity_property_list);
		}

		if (findViewById(R.id.property_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((PropertyListFragment) getSupportFragmentManager()
					.findFragmentById(R.id.property_list))
					.setActivateOnItemClick(true);
		}

		// TODO: If exposing deep links into your app, handle intents here.
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.property_detail, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_new:
	            addProperty();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	/**
	 * Callback method from {@link PropertyListFragment.Callbacks} indicating
	 * that the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(PropertyDetailFragment.ARG_ITEM_ID, id);
			PropertyDetailFragment fragment = new PropertyDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.property_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, PropertyDetailActivity.class);
			detailIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}
	
	public void openAddProperty(View view) {
		addProperty();
	}
	
	public void addProperty(){
		Intent intent = new Intent(this, AddPropertyActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Open Property Reports - onClickListener
	 * @param view
	 */
	public void openPropertyReport(View view)
	{
		SharedPreferences prefs = this.getSharedPreferences(
			      "com.nomoreboundary.myrental.pro", Context.MODE_PRIVATE);
		prefs.edit().putString("property_id", getIntent()
				.getStringExtra(PropertyDetailFragment.ARG_ITEM_ID)).apply();
		
		Intent intent = new Intent(this, PropertyReportActivity.class);
		intent.putExtra("property_id", getIntent()
				.getStringExtra(PropertyDetailFragment.ARG_ITEM_ID));
		startActivity(intent);
	}
}
