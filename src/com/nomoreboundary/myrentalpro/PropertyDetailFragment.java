package com.nomoreboundary.myrentalpro;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nomoreboundary.myrentalpro.cashflow.ManageCashFlowActivity;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Property;
import com.nomoreboundary.myrentalpro.expenses.ManageExpenseActivity;
import com.nomoreboundary.myrentalpro.loan.ManageLoanActivity;
import com.nomoreboundary.myrentalpro.property.DeletePropertyActivity;
import com.nomoreboundary.myrentalpro.property.EditPropertyActivity;
import com.nomoreboundary.myrentalpro.rent.ManageRentActivity;
import com.nomoreboundary.myrentalpro.report.PropertyReportActivity;
import com.nomoreboundary.myrentalpro.tenant.EditTenantActivity;
import com.nomoreboundary.myrentalpro.util.AddressFormatter;

/**
 * A fragment representing a single Property detail screen. This fragment is
 * either contained in a {@link PropertyListActivity} in two-pane mode (on
 * tablets) or a {@link PropertyDetailActivity} on handsets.
 */
public class PropertyDetailFragment extends Fragment implements OnClickListener {

	private static final String TAG = "PropertyDetailFragment";
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "property_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private Property mProperty;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public PropertyDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			// DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

			PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(
					getActivity());

			Log.d(TAG, "ARG_ITEM_ID=" + getArguments().getString(ARG_ITEM_ID));
			mProperty = dbHelper.getProperty(getArguments().getString(
					ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_property_detail,
				container, false);

		// Show the content as text in a TextView.
		if (mProperty != null) {
			((TextView) rootView.findViewById(R.id.property_detail))
					.setText(mProperty.getNickname());

			Address address = mProperty.getAddress();
			String formattedAddress = AddressFormatter
					.getFormattedAddress(address.toString());
			((TextView) rootView.findViewById(R.id.property_detail_address))
					.setText(formattedAddress);
		}

		// Add Rent
		ImageButton btnAddRent = (ImageButton) rootView
				.findViewById(R.id.imgBtnRent);
		btnAddRent.setOnClickListener(this);

		// Edit Tenant
		ImageButton btnPropertyDetailTenant = (ImageButton) rootView
				.findViewById(R.id.imgBtnTenant);
		btnPropertyDetailTenant.setOnClickListener(this);

		// Edit Property
		ImageButton btnPropertyDetailDelete = (ImageButton) rootView
				.findViewById(R.id.imgBtnEditProperty);
		btnPropertyDetailDelete.setOnClickListener(this);

		// Delete Property
		ImageButton btnPropertyDetailEdit = (ImageButton) rootView
				.findViewById(R.id.imgBtnDeleteProperty);
		btnPropertyDetailEdit.setOnClickListener(this);

		// Cash Flow
		ImageButton btnPropertyDetailCashFlow = (ImageButton) rootView
				.findViewById(R.id.imgBtnPropertyDetailCashFlow);
		btnPropertyDetailCashFlow.setOnClickListener(this);

		// Loan
		ImageButton btnPropertyDetailLoan = (ImageButton) rootView
				.findViewById(R.id.imgBtnPropertyDetailLoan);
		btnPropertyDetailLoan.setOnClickListener(this);

		// Manage Expenses
		ImageButton btnManageExpenses = (ImageButton) rootView
				.findViewById(R.id.imgBtnExpenses);
		btnManageExpenses.setOnClickListener(this);

		// Open Property Report
		ImageButton btnPropertyReport = (ImageButton) rootView
				.findViewById(R.id.imgBtnPropertyReports);
		btnPropertyReport.setOnClickListener(this);

		// Open Property Report
		ImageButton btnPropertyToDo = (ImageButton) rootView
				.findViewById(R.id.imgBtnMoveInOut);
		btnPropertyReport.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgBtnPropertyDetailCashFlow:
			openCashFlow();
			break;
		case R.id.imgBtnPropertyDetailLoan:
			openLoan();
			break;
		case R.id.imgBtnTenant:
			openEditTenant();
			break;
		case R.id.imgBtnDeleteProperty:
			openDeleteProperty();
			break;
		case R.id.imgBtnEditProperty:
			openEditProperty();
			break;
		case R.id.imgBtnRent:
			openRentActivity();
			break;
		case R.id.imgBtnExpenses:
			openManageExpenses();
			break;
		case R.id.imgBtnPropertyReports:
			openPropertyReport();
			break;
		case R.id.imgBtnMoveInOut:
			openToDoList();
			break;
		}
	}

	public void openRentActivity() {
		Intent intent = new Intent(getActivity(), ManageRentActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));

		startActivity(intent);
	}

	private void openCashFlow() {
		Intent intent = new Intent(getActivity(), ManageCashFlowActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	private void openLoan() {
		Intent intent = new Intent(getActivity(), ManageLoanActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	private void openEditTenant() {
		Intent intent = new Intent(getActivity(), EditTenantActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	private void openDeleteProperty() {
		Intent intent = new Intent(getActivity(), DeletePropertyActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	private void openEditProperty() {
		Intent intent = new Intent(getActivity(), EditPropertyActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	public void openManageExpenses() {
		Intent intent = new Intent(getActivity(), ManageExpenseActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	public void openPropertyReport() {
		SharedPreferences prefs = getActivity().getSharedPreferences(
				"com.nomoreboundary.myrental.pro", Context.MODE_PRIVATE);
		prefs.edit()
				.putString("property_id", getArguments().getString(ARG_ITEM_ID))
				.apply();

		Intent intent = new Intent(getActivity(), PropertyReportActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

	public void openToDoList() {
		Intent intent = new Intent(getActivity(), PropertyReportActivity.class);
		intent.putExtra("property_id", getArguments().getString(ARG_ITEM_ID));
		startActivity(intent);
	}

}
