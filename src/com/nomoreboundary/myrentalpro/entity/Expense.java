package com.nomoreboundary.myrentalpro.entity;

public class Expense {
	String id;
	String type;
	String description;
	String date;
	double amount;
	
	public Expense () {
		
	}
	
	public Expense (String id, String type, String description, String date, double amount) {
		this.id = id;
		this.type = type;
		this.description = description;
		this.date = date;
		this.amount = amount;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString()
	{
		return "ID=" + this.id + ",Date=" + this.date + ",Amount=" + this.amount + ",Type=" + this.type + ",Description=" + this.description;
	}

}
