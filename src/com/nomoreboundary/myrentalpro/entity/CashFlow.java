package com.nomoreboundary.myrentalpro.entity;

public class CashFlow {
	
	String id;
	double grossPotentialIncome;
	double vacancyLoss;  // % vacancy and collection loss
	double otherIncome;
	double effectiveGrossIncome;
	double grossOperatingIncome;
	double totalOperatingExpenses;
	double netOperatingIncome;
	double reservesReplacementAccount;
	double debtService;
	double beforeTaxCashFlow;
	
	@Override
	public String toString()
	{
		return "ID=" + this.id 
				+ "GPI=" + this.grossPotentialIncome 
				+ ",vac=" + this.vacancyLoss 
				+ ",OI=" + this.otherIncome 
				+ ",TOE=" + this.totalOperatingExpenses 
				+ ",RRA=" + this.reservesReplacementAccount 
				+ ",DS=" + this.debtService;
	} 

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getGrossPotentialIncome() {
		return grossPotentialIncome;
	}

	public void setGrossPotentialIncome(double grossPotentialIncome) {
		this.grossPotentialIncome = grossPotentialIncome;
	}

	public double getVacancyLoss() {
		return vacancyLoss;
	}

	public void setVacancyLoss(double vacancyLoss) {
		this.vacancyLoss = vacancyLoss;
	}

	public double getOtherIncome() {
		return otherIncome;
	}

	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}

	public double getEffectiveGrossIncome() {
		return effectiveGrossIncome;
	}

	public void setEffectiveGrossIncome(double effectiveGrossIncome) {
		this.effectiveGrossIncome = effectiveGrossIncome;
	}

	public double getGrossOperatingIncome() {
		return grossOperatingIncome;
	}

	public void setGrossOperatingIncome(double grossOperatingIncome) {
		this.grossOperatingIncome = grossOperatingIncome;
	}

	public double getTotalOperatingExpenses() {
		return totalOperatingExpenses;
	}

	public void setTotalOperatingExpenses(double totalOperatingExpenses) {
		this.totalOperatingExpenses = totalOperatingExpenses;
	}

	public double getNetOperatingIncome() {
		return netOperatingIncome;
	}

	public void setNetOperatingIncome(double netOperatingIncome) {
		this.netOperatingIncome = netOperatingIncome;
	}

	public double getReservesReplacementAccount() {
		return reservesReplacementAccount;
	}

	public void setReservesReplacementAccount(double reservesReplacementAccount) {
		this.reservesReplacementAccount = reservesReplacementAccount;
	}

	public double getDebtService() {
		return debtService;
	}

	public void setDebtService(double debtService) {
		this.debtService = debtService;
	}

	public double getBeforeTaxCashFlow() {
		return beforeTaxCashFlow;
	}

	public void setBeforeTaxCashFlow(double beforeTaxCashFlow) {
		this.beforeTaxCashFlow = beforeTaxCashFlow;
	}
}
