package com.nomoreboundary.myrentalpro.entity;

import android.location.Address;

/**
 * Define ONE manageable rental unit of property
 * @author rajeshprem
 *
 * Create an empty property,  Add an address, tenant and loan to it
 */
public class Property {

	public String id;
	public String nickname; 
	Address address;
	Tenant tenant;
	Loan loan = null;
	double rent = 0.0;
	
	public double getRent() {
		return rent;
	}

	public void setRent(double rent) {
		this.rent = rent;
	}
	
	public Property()  {
		
	}

	/**
	 * Create an empty property
	 * @param nickname
	 */
	public Property (String nickname)
	{
		this.nickname = nickname;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}
	
	public boolean hasLoan()
	{
		if (this.loan == null)
			return true;
		else
			return false;
	}
}
