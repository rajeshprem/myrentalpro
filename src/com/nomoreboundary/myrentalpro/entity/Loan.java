package com.nomoreboundary.myrentalpro.entity;

public class Loan {
	double purchasePrice;
	double interestRate;
	double downPayment;
	int term;
	double hoa;
	String id;
	String startDate;
	
	public Loan() {
		
	}
	
	public Loan (String id, double purchasePrice, double interestRate, double downPayment, int term) {
		this.id = id;
		this.purchasePrice = purchasePrice;
		this.interestRate = interestRate;
		this.downPayment = downPayment;
		this.term = term;
	}
	
	/**
	 * Optional HOA
	 */
	public Loan (String id, double purchasePrice, double interestRate, double downPayment, int term, double hoa) {
		this.id = id;
		this.purchasePrice = purchasePrice;
		this.interestRate = interestRate;
		this.downPayment = downPayment;
		this.term = term;
		this.hoa = hoa;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getDownPayment() {
		return downPayment;
	}

	public void setDownPayment(double downPayment) {
		this.downPayment = downPayment;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public double getHoa() {
		return hoa;
	}

	public void setHoa(double hoa) {
		this.hoa = hoa;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public String toString()
	{
		return "ID=" + this.id 
				+ ",PP=" + this.purchasePrice 
				+ ",IR=" + this.interestRate 
				+ ",DP=" + this.downPayment 
				+ ",Term=" + this.term 
				+ ",HOA=" + this.hoa;
	}
	
}
