package com.nomoreboundary.myrentalpro.entity;

public class Rent {
	
	String id;
	String tenant;
	String date;
	double amount;
	
	public Rent() {
		
	}
	
	public Rent (String id, String tenant, String date, double amount) {
		this.id = id;
		this.tenant = tenant;
		this.date = date;
		this.amount = amount;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTenant() {
		return tenant;
	}
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString()
	{
		return "ID=" + this.id + ",Date=" + this.date + ",Amount=" + this.amount + ",Tenant=" + this.tenant;
	}

}
