package com.nomoreboundary.myrentalpro.entity;

public class Tenant {
	String name;
	double rent;
	
	public Tenant (String name)
	{
		this.name = name;
	}
	
	public Tenant (String name, double rent)
	{
		this.rent = rent;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRent() {
		return rent;
	}

	public void setRent(double rent) {
		this.rent = rent;
	}
}
