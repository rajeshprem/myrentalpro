package com.nomoreboundary.myrentalpro.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.entity.CashFlow;


public class CashFlowDatabaseHelper extends SQLiteOpenHelper {
	
	private static final String TAG = "CashFlowDatabaseHelper";
	private static final String DATABASE_NAME = "cashflow.db";
	private static final int DATABASE_VERSION = MyRentalProApplication.DATABASE_VERSION;

	public static final String TABLE_CASHFLOW = "cashflow";
	public static final String COLUMN_PROPERTY_ID = "id";
	public static final String COLUMN_GPI = "gpi";
	public static final String COLUMN_VACANCY_RATE = "vacancy_rate";
	public static final String COLUMN_OTHER_INCOME = "other_income";
	public static final String COLUMN_OPERATING_EXPENSES = "operating_expenses";
	public static final String COLUMN_RRA = "rra";
	public static final String COLUMN_DEBT_SERVICE = "debt_service";

	private static final String[] COLUMNS = { COLUMN_PROPERTY_ID, COLUMN_GPI, COLUMN_VACANCY_RATE,
		COLUMN_OTHER_INCOME, COLUMN_OPERATING_EXPENSES, COLUMN_RRA, COLUMN_DEBT_SERVICE   };
	
	// Database creation sql statement
		private static final String DATABASE_CREATE = "create table "
					+ TABLE_CASHFLOW + "(" 
					+ COLUMN_PROPERTY_ID  + " integer not null, " 
					+ COLUMN_GPI + " real, " 
					+ COLUMN_VACANCY_RATE + " real, " 
					+ COLUMN_OTHER_INCOME + " real, " 
					+ COLUMN_OPERATING_EXPENSES + " real, " 
					+ COLUMN_RRA + " real, " 
					+ COLUMN_DEBT_SERVICE + " real);";

	public CashFlowDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG,
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CASHFLOW);
		onCreate(db);
	}
	
	public void addCashFlowParams(CashFlow cashflow) {
		Log.d(TAG, "Adding propertyID=" + cashflow.getId());

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		
		values.put(COLUMN_PROPERTY_ID, cashflow.getId());
		values.put(COLUMN_GPI, cashflow.getGrossPotentialIncome());
		values.put(COLUMN_VACANCY_RATE, cashflow.getVacancyLoss());
		values.put(COLUMN_OTHER_INCOME, cashflow.getOtherIncome());
		values.put(COLUMN_OPERATING_EXPENSES, cashflow.getTotalOperatingExpenses());
		values.put(COLUMN_RRA, cashflow.getReservesReplacementAccount());
		values.put(COLUMN_DEBT_SERVICE, cashflow.getDebtService());
		
		Log.d(TAG, "Values=" + values.toString());

		db.insert(TABLE_CASHFLOW, // table
				null, // nullColumnHack
				values); // key/value -> keys = column names/ values = column

		db.close();
	}

	public void updateCashFlowParams(CashFlow cashflow) {
	
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_PROPERTY_ID, cashflow.getId());
		values.put(COLUMN_GPI, cashflow.getGrossPotentialIncome());
		values.put(COLUMN_VACANCY_RATE, cashflow.getVacancyLoss());
		values.put(COLUMN_OTHER_INCOME, cashflow.getOtherIncome());
		values.put(COLUMN_OPERATING_EXPENSES, cashflow.getTotalOperatingExpenses());
		values.put(COLUMN_RRA, cashflow.getReservesReplacementAccount());
		values.put(COLUMN_DEBT_SERVICE, cashflow.getDebtService());
		
		Log.d(TAG, "Update Values=" + values.toString());
		db.update(TABLE_CASHFLOW, values, COLUMN_PROPERTY_ID +" = ?", new String[] { cashflow.getId() });
		Log.d(TAG, "DB Updated");
		db.close();
	}
	
	public CashFlow getCashFlowParams(String id)
	{
		 // 1. get reference to readable DB
	    SQLiteDatabase db = this.getReadableDatabase();
	 
	    // 2. build query
	    Cursor cursor = 
	            db.query(TABLE_CASHFLOW, // a. table
	            COLUMNS, // b. column names
	            " id = ?", // c. selections 
	            new String[] { id }, // d. selections args
	            null, // e. group by
	            null, // f. having
	            null, // g. order by
	            null); // h. limit
	 
	    // 3. if we got results get the first one
	    if (cursor != null)
	        cursor.moveToFirst();
	 
	    // 4. build loan object
		CashFlow cf = new CashFlow();
	    cf.setId(String.valueOf(cursor.getInt(cursor.getColumnIndex(COLUMN_PROPERTY_ID))));
	    cf.setGrossPotentialIncome(cursor.getDouble(cursor.getColumnIndex(COLUMN_GPI)));
	    cf.setVacancyLoss(cursor.getDouble(cursor.getColumnIndex(COLUMN_VACANCY_RATE)));
	    cf.setOtherIncome(cursor.getDouble(cursor.getColumnIndex(COLUMN_OTHER_INCOME)));
	    cf.setTotalOperatingExpenses(cursor.getDouble(cursor.getColumnIndex(COLUMN_OPERATING_EXPENSES)));
	    cf.setReservesReplacementAccount(cursor.getDouble(cursor.getColumnIndex(COLUMN_RRA)));
	    cf.setDebtService(cursor.getDouble(cursor.getColumnIndex(COLUMN_DEBT_SERVICE)));
	 
	    //log 
	    Log.d("getCashFlow("+id+")", cf.toString());
	    db.close();
		return cf;
	}
	
	public CashFlow getEmptyCashFlowParams()
	{
		CashFlow cf = new CashFlow();
	    cf.setId(String.valueOf(0));
	    cf.setGrossPotentialIncome(0.0);
	    cf.setVacancyLoss(0.0);
	    cf.setOtherIncome(0.0);
	    cf.setTotalOperatingExpenses(0.0);
	    cf.setReservesReplacementAccount(0.0);
	    cf.setDebtService(0.0);
	 
		return cf;
	}
	
	/**
	 * Returns true if cashflow parameters exist for a property.  There can be only one row.
	 * @param id
	 * @return
	 */
	public boolean checkIfCashFlowParamsExist(String id)
	{
		 // 1. get reference to readable DB
	    SQLiteDatabase db = this.getReadableDatabase();
	 
	    // 2. build query
	    Cursor cursor = 
	            db.query(TABLE_CASHFLOW, // a. table
	            COLUMNS, // b. column names
	            " id = ?", // c. selections 
	            new String[] { id }, // d. selections args
	            null, // e. group by
	            null, // f. having
	            null, // g. order by
	            null); // h. limit
	 
	    // 3. if we got results get the first one
	    Log.d(TAG, "Check if cashflow params exists=" + cursor.moveToFirst());
	    db.close();
	    return cursor.moveToFirst();
	}


}
