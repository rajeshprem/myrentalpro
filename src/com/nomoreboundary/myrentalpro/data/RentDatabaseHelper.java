package com.nomoreboundary.myrentalpro.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.joda.time.LocalDate;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.entity.Property;
import com.nomoreboundary.myrentalpro.entity.Rent;

public class RentDatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = "RentDatabaseHelper";
	private static final String DATABASE_NAME = "rent.db";
	private static final int DATABASE_VERSION = MyRentalProApplication.DATABASE_VERSION;

	public static final String TABLE_RENT = "rent";
	public static final String COLUMN_PROPERTY_ID = "property_id";
	public static final String COLUMN_TENANT = "tenant";
	public static final String COLUMN_AMOUNT = "amount";
	public static final String COLUMN_DATE = "date";

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table " + TABLE_RENT
			+ "(" + COLUMN_PROPERTY_ID + " integer not null, " + COLUMN_TENANT
			+ " text, " + COLUMN_AMOUNT + " integer not null, " + COLUMN_DATE
			+ " text);";

	public RentDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RENT);
		onCreate(db);
	}

	/**
	 * Add Rent entry to table
	 * 
	 * @param property
	 * @param rent
	 * @param date
	 */
	public void addRent(Property property, double rent, String date) {
		Log.d(TAG, "Adding " + property.getNickname());

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(COLUMN_PROPERTY_ID, Integer.parseInt(property.getId()));
		values.put(COLUMN_TENANT, property.getTenant().getName());
		values.put(COLUMN_AMOUNT, rent);
		values.put(COLUMN_DATE, date);

		Log.d(TAG, "Values=" + values.toString());

		db.insert(TABLE_RENT, // table
				null, // nullColumnHack
				values); // key/value -> keys = column names/ values = column

		db.close();
	}

	/**
	 * Add recurring rents to the database
	 * @param property
	 * @param rent
	 * @param fromDate
	 * @param toDate
	 */
	public void addRent(Property property, double rent, String fromDate,
			String toDate) {
		Log.d(TAG, "Adding recurring rent to: " + property.getNickname());

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(COLUMN_PROPERTY_ID, Integer.parseInt(property.getId()));
		values.put(COLUMN_TENANT, property.getTenant().getName());
		values.put(COLUMN_AMOUNT, rent);

		LocalDate dateStart = new LocalDate(fromDate);
		LocalDate dateEnd = new LocalDate(toDate);
		// month by month :
		while (dateStart.isBefore(dateEnd)) {

			Log.d(TAG, "Adding rent to date: " + dateStart);
			values.put(COLUMN_DATE, dateStart.toString());

			Log.d(TAG, "Values=" + values.toString());

			db.insert(TABLE_RENT, null, values);
			dateStart = dateStart.plusMonths(1);
		}
		db.close();
		
		Log.d(TAG, "Adding Recurring Rent... [DONE]");

	}

	/**
	 * Return a list of rents paid on a property
	 * 
	 * @return
	 */
	public List<Rent> getAllRentsForProperty(String propertyId) {

		SQLiteDatabase db = this.getWritableDatabase();

		List<Rent> rents = new ArrayList<Rent>();

		// 1. build the query
		// select * from rent where property_id=1
		String query = "SELECT " + COLUMN_AMOUNT + "," + COLUMN_TENANT + ","
				+ COLUMN_DATE + " FROM " + TABLE_RENT + " where "
				+ COLUMN_PROPERTY_ID + "=" + propertyId;

		// 2. get reference to writable DB
		Cursor cursor = db.rawQuery(query, null);

		// 3. go over each row, get the rent amount and add it to list
		Rent rent = null;
		if (cursor.moveToFirst()) {
			do {
				rent = new Rent();
				rent.setId(propertyId);
				rent.setAmount(Double.parseDouble(cursor.getString(cursor
						.getColumnIndex(COLUMN_AMOUNT))));
				rent.setDate(cursor.getString(cursor
						.getColumnIndex(COLUMN_DATE)));
				rent.setTenant(cursor.getString(cursor
						.getColumnIndex(COLUMN_TENANT)));
				rents.add(rent);
			} while (cursor.moveToNext());
		}

		Log.d(TAG, "getAllRents(): # of records: " + rents.size());
		cursor.close();
		db.close();
		return rents;
	}

	/**
	 * Delete a rent entry
	 * 
	 * @param rent
	 */
	public void deleteRent(Rent rent) {

		// 1. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();

		// 2. delete
		int rowCount = db.delete(TABLE_RENT, // table name
				COLUMN_PROPERTY_ID + "=? and " + COLUMN_TENANT + "=? and "
						+ COLUMN_DATE + "=? and " + COLUMN_AMOUNT + "=?", // selections
				new String[] { rent.getId(), rent.getTenant(), rent.getDate(),
						String.valueOf(rent.getAmount()) }); // selections //
																// args

		db.close();

		Log.d("deleteRent", "Rows Deleted: " + rowCount);
		Log.d("deleteRent", rent.toString());

	}

	/**
	 * Get total rent for current year
	 */
	public String getTotalRentForCurrentYear(String propertyId) {
		String query = "SELECT SUM(" + COLUMN_AMOUNT + ") FROM " + TABLE_RENT
				+ " where " + COLUMN_PROPERTY_ID + "=" + propertyId;
		// + " and "
		// + "strftime(\'%Y-%m-%d\',\'" + "2014-08-01" + "\')"
		// + " between date(\'start of year\') and date(\'now\')";

		Log.d(TAG, "Query=" + query);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String totalRent = "NA";

		if (cursor != null) {
			cursor.moveToFirst();
			totalRent = String.valueOf(cursor.getInt(0));
		}

		Log.d(TAG, "Total Rent for Current Year=" + totalRent);

		return totalRent;

	}

	public String getTotalRentForAllTime(String propertyId) {
		String query = "SELECT SUM(" + COLUMN_AMOUNT + ") FROM " + TABLE_RENT
				+ " where " + COLUMN_PROPERTY_ID + "=" + propertyId;

		Log.d(TAG, "Query=" + query);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String totalRent = "NA";

		if (cursor != null) {
			cursor.moveToFirst();
			totalRent = String.valueOf(cursor.getInt(0));
		}

		Log.d(TAG, "Total Rent for All Time=" + totalRent);

		return totalRent;

	}
}
