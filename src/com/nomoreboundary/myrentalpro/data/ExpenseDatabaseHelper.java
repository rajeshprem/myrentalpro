package com.nomoreboundary.myrentalpro.data;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.entity.Expense;


public class ExpenseDatabaseHelper extends SQLiteOpenHelper {
	
	private static final String TAG = "ExpenseDatabaseHelper";
	private static final String DATABASE_NAME = "expense.db";
	private static final int DATABASE_VERSION = MyRentalProApplication.DATABASE_VERSION;


	public static final String TABLE_EXPENSE = "expense";
	public static final String COLUMN_PROPERTY_ID = "property_id";
	public static final String COLUMN_TYPE = "type";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_AMOUNT = "amount";
	public static final String COLUMN_DATE = "date";
	
	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
				+ TABLE_EXPENSE + "(" 
				+ COLUMN_PROPERTY_ID + " integer not null, " 
				+ COLUMN_TYPE + " text, " 
				+ COLUMN_DESCRIPTION + " text, "
				+ COLUMN_AMOUNT + " integer not null, " 
				+ COLUMN_DATE + " text);";


	public ExpenseDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG,
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSE);
		onCreate(db);
	}
	
	/**
	 * Add Expense entry to table
	 */
	public void addExpense(String propertyId, String expenseType, String expenseDescription, double amount, String date) {
		Log.d(TAG, "Adding Property with ID=" + propertyId);

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		
		values.put(COLUMN_PROPERTY_ID, propertyId);
		values.put(COLUMN_TYPE, expenseType);
		values.put(COLUMN_DESCRIPTION, expenseDescription);
		values.put(COLUMN_AMOUNT, amount);
		values.put(COLUMN_DATE, date);
		
		Log.d(TAG, "Values=" + values.toString());

		db.insert(TABLE_EXPENSE, // table
				null, // nullColumnHack
				values); // key/value -> keys = column names/ values = column

		db.close();
	}
	
	/**
	 * Return a list of expenses paid on a property
	 * @return
	 */
	public List<Expense> getAllExpensesForProperty(String propertyId) {

		List<Expense> expenses = new ArrayList<Expense>();

		// 1. build the query
		// select * from expense where property_id=1
		String query = "SELECT " 
				+ COLUMN_AMOUNT + "," 
				+ COLUMN_TYPE + "," 
				+ COLUMN_DESCRIPTION + "," 
				+ COLUMN_DATE  
				+ " FROM " + TABLE_EXPENSE
				+ " where " + COLUMN_PROPERTY_ID + "=" + propertyId;

		// 2. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// 3. go over each row, get the expense amount and add it to list
		Expense expense = null;
		if (cursor.moveToFirst()) {
			do {
				expense = new Expense();
				expense.setId(propertyId);
				expense.setAmount(Double.parseDouble(cursor.getString(cursor.getColumnIndex(COLUMN_AMOUNT))));
				expense.setDate(cursor.getString(cursor.getColumnIndex(COLUMN_DATE)));
				expense.setType(cursor.getString(cursor.getColumnIndex(COLUMN_TYPE)));
				expense.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
				expenses.add(expense);
			} while (cursor.moveToNext());
		}

		Log.d(TAG, "getAllExpenses(): # of records: " + expenses.size());
		cursor.close();
		db.close();
		return expenses;
	}
	
	/**
	 * Delete a expense entry
	 * @param expense
	 */
	public void deleteExpense(Expense expense) {

		// 1. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();

		// 2. delete
		int rowCount = db.delete(TABLE_EXPENSE, // table name
				COLUMN_PROPERTY_ID + "=? and " + COLUMN_TYPE + "=? and " + COLUMN_DATE + "=? and " + COLUMN_AMOUNT + "=?", // selections
				new String[] { expense.getId(), expense.getType(), expense.getDate(), String.valueOf(expense.getAmount()) }); // selections // args
																
		db.close();
		
		Log.d("deleteExpense", "Rows Deleted: "+ rowCount);
		Log.d("deleteExpense", expense.toString());

	}
	
	/**
	 * Get total expenses for current year
	 */
	public String getTotalExpenseForCurrentYear(String propertyId) {
		String query = "SELECT SUM(" + COLUMN_AMOUNT + ") FROM " + TABLE_EXPENSE
				+ " where " + COLUMN_PROPERTY_ID + "=" + propertyId;
		       // + " and "
			  //	+ "strftime(\'%Y-%m-%d\',\'" + "2014-08-01" + "\')"
			//	+ " between date(\'start of year\') and date(\'now\')";

		Log.d(TAG, "Query=" + query);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String totalExpense = "NA";
		
		if (cursor != null) {
			cursor.moveToFirst();
		    totalExpense = String.valueOf(cursor.getInt(0));
		} else {
			totalExpense = "0";
		}

		Log.d(TAG, "Total Expense for Current Year=" + totalExpense);

		return totalExpense;

	}
	
	public String getTotalExpenseForAllTime(String propertyId) {
		String query = "SELECT SUM(" + COLUMN_AMOUNT + ") FROM " + TABLE_EXPENSE
				+ " where " + COLUMN_PROPERTY_ID + "=" + propertyId;

		Log.d(TAG, "Query=" + query);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		String totalExpense = "NA";
		
		if (cursor != null) {
			cursor.moveToFirst();
		    totalExpense = String.valueOf(cursor.getInt(0));
		} else {
			totalExpense = "0";
		}

		Log.d(TAG, "Total Expense for All Time=" + totalExpense);

		return totalExpense;

	}
}
