package com.nomoreboundary.myrentalpro.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.entity.Loan;

public class LoanDatabaseHelper extends SQLiteOpenHelper {
	
	private static final String TAG = "LoanDatabaseHelper";
	private static final String DATABASE_NAME = "loan.db";
	private static final int DATABASE_VERSION = MyRentalProApplication.DATABASE_VERSION;

	public static final String TABLE_LOAN = "loan";
	public static final String COLUMN_PROPERTY_ID = "id";
	public static final String COLUMN_PURCHASE_PRICE = "purchase_price";
	public static final String COLUMN_INTEREST_RATE = "interest_rate";
	public static final String COLUMN_DOWN_PAYMENT = "down_payment";
	public static final String COLUMN_TERM = "term";
	public static final String COLUMN_HOA = "hoa";

	private static final String[] COLUMNS = { COLUMN_PROPERTY_ID, COLUMN_PURCHASE_PRICE, COLUMN_INTEREST_RATE,
		COLUMN_DOWN_PAYMENT, COLUMN_TERM, COLUMN_HOA  };
	
	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
				+ TABLE_LOAN + "(" 
				+ COLUMN_PROPERTY_ID  + " integer not null, " 
				+ COLUMN_PURCHASE_PRICE + " real, " 
				+ COLUMN_INTEREST_RATE + " real, " 
				+ COLUMN_DOWN_PAYMENT + " real, " 
				+ COLUMN_TERM + " integer, " 
				+ COLUMN_HOA + " real);";

	public LoanDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG,
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOAN);
		onCreate(db);
	}
	
	/**
	 * Add a loan to the property for the first time
	 */
	public void addLoan(Loan loan) {
		Log.d(TAG, "Adding propertyID=" + loan.getId());

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		
		values.put(COLUMN_PROPERTY_ID, loan.getId());
		values.put(COLUMN_PURCHASE_PRICE, loan.getPurchasePrice());
		values.put(COLUMN_INTEREST_RATE, loan.getInterestRate());
		values.put(COLUMN_DOWN_PAYMENT, loan.getDownPayment());
		values.put(COLUMN_TERM, loan.getTerm());
		values.put(COLUMN_HOA, loan.getHoa());
		
		Log.d(TAG, "Values=" + values.toString());

		db.insert(TABLE_LOAN, // table
				null, // nullColumnHack
				values); // key/value -> keys = column names/ values = column

		db.close();
	}
	
	/**
	 * Update same loan for property ID - Only one loan allowed per property
	 */
	public void updateLoan(Loan loan) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_PROPERTY_ID, loan.getId());
		values.put(COLUMN_PURCHASE_PRICE, loan.getPurchasePrice());
		values.put(COLUMN_INTEREST_RATE, loan.getInterestRate());
		values.put(COLUMN_DOWN_PAYMENT, loan.getDownPayment());
		values.put(COLUMN_TERM, loan.getTerm());
		values.put(COLUMN_HOA, loan.getHoa());
		
		Log.d(TAG, "Update Values=" + values.toString());
		db.update(TABLE_LOAN, values, COLUMN_PROPERTY_ID +" = ?", new String[] { loan.getId() });
		Log.d(TAG, "DB Updated");
		db.close();
	}
	
	public Loan getLoan(String id)
	{
		    // 1. get reference to readable DB
		    SQLiteDatabase db = this.getReadableDatabase();
		 
		    // 2. build query
		    Cursor cursor = 
		            db.query(TABLE_LOAN, // a. table
		            COLUMNS, // b. column names
		            " id = ?", // c. selections 
		            new String[] { id }, // d. selections args
		            null, // e. group by
		            null, // f. having
		            null, // g. order by
		            null); // h. limit
		 
		    // 3. if we got results get the first one
		    if (cursor != null)
		        cursor.moveToFirst();
		 
		    // 4. build loan object
		    Loan loan = new Loan();
		    loan.setId(String.valueOf(cursor.getInt(cursor.getColumnIndex(COLUMN_PROPERTY_ID))));
		    loan.setPurchasePrice(cursor.getDouble(cursor.getColumnIndex(COLUMN_PURCHASE_PRICE)));
		    loan.setInterestRate(cursor.getDouble(cursor.getColumnIndex(COLUMN_INTEREST_RATE)));
		    loan.setDownPayment(cursor.getDouble(cursor.getColumnIndex(COLUMN_DOWN_PAYMENT)));
		    loan.setTerm(cursor.getInt(cursor.getColumnIndex(COLUMN_TERM)));
		    loan.setHoa(cursor.getDouble(cursor.getColumnIndex(COLUMN_HOA)));
		 
		    //log 
		    Log.d("getLoan("+id+")", loan.toString());
		    db.close();
		    return loan;
		}
	
	/**
	 * Return an empty loan object
	 * @return
	 */
	public Loan getEmptyLoan() 
	{
		 Loan loan = new Loan();
		    loan.setId(String.valueOf(0));
		    loan.setPurchasePrice(0.00);
		    loan.setInterestRate(0.0);
		    loan.setDownPayment(0.0);
		    loan.setTerm(0);
		    loan.setHoa(0.0);
		    return loan;
	}
	
	/**
	 * Checks for a loan entry on a property
	 * @param id
	 * @return
	 */
	public boolean checkLoan(String id)
	{
		    // 1. get reference to readable DB
		    SQLiteDatabase db = this.getReadableDatabase();
		 
		    // 2. build query
		    Cursor cursor = 
		            db.query(TABLE_LOAN, // a. table
		            COLUMNS, // b. column names
		            " id = ?", // c. selections 
		            new String[] { id }, // d. selections args
		            null, // e. group by
		            null, // f. having
		            null, // g. order by
		            null); // h. limit
		 
		    // 3. if we got results get the first one
		    Log.d(TAG, "Check if loan exists=" + cursor.moveToFirst());
		    db.close();
		    return cursor.moveToFirst();
		}
}
