package com.nomoreboundary.myrentalpro.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Address;
import android.util.Log;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.MyRentalProApplication;
import com.nomoreboundary.myrentalpro.entity.Loan;
import com.nomoreboundary.myrentalpro.entity.Property;
import com.nomoreboundary.myrentalpro.entity.Tenant;
import com.nomoreboundary.myrentalpro.util.AddressFormatter;

public class PropertyDatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = "DatabaseHelper";
	private static final String DATABASE_NAME = MyRentalProApplication.DATABASE_NAME;
	private static final int DATABASE_VERSION = MyRentalProApplication.DATABASE_VERSION;

	public static final String TABLE_PROPERTY = "property";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NICKNAME = "nickname";
	public static final String COLUMN_TENANT = "tenant";
	public static final String COLUMN_ADDRESS = "address";
	public static final String COLUMN_RENT = "rent";

	private static final String[] COLUMNS = { COLUMN_ID, COLUMN_NICKNAME, COLUMN_TENANT,
			COLUMN_ADDRESS, COLUMN_RENT };

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_PROPERTY + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_NICKNAME
			+ " text not null, " + COLUMN_TENANT + " text, " + COLUMN_ADDRESS
			+ " text not null, " + COLUMN_RENT + " real);";

	public PropertyDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(PropertyDatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY);
		onCreate(db);
	}

	/**
	 * Add a property to the database
	 * 
	 * @param property
	 */
	public void addProperty(Property property) {
		Log.d(TAG, "Adding " + property.getNickname());

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_NICKNAME, property.getNickname());
		values.put(COLUMN_TENANT, property.getTenant().getName());
		Log.d(TAG, property.getAddress().toString());
		
		values.put(COLUMN_ADDRESS, property.getAddress().toString());
		values.put(COLUMN_RENT, property.getRent());

		db.insert(TABLE_PROPERTY, // table
				null, // nullColumnHack
				values); // key/value -> keys = column names/ values = column
							// values

		Log.d(TAG, "Values=" + values.toString());

		db.close();
	}

	public Property getProperty(String id) {

		if (id == null) {
			Log.e(TAG, "property id is NULL!!");
		}
		// 1. get reference to readable DB
		SQLiteDatabase db = this.getReadableDatabase();

		// 2. build query
		Cursor cursor = db.query(TABLE_PROPERTY, // a. table
				COLUMNS, // b. column names
				" _id = ?", // c. selections
				new String[] { id }, // d. selections args
				null, // e. group by
				null, // f. having
				null, // g. order by
				null); // h. limit

		// 3. if we got results get the first one
		if (cursor != null && cursor.moveToFirst()) 
		{
			// 4. build property object
			Property property = new Property(cursor.getString(cursor.getColumnIndex(COLUMN_NICKNAME))); // nickname
			property.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));

			Tenant tenant = new Tenant(cursor.getString(cursor.getColumnIndex(COLUMN_TENANT)));
			property.setTenant(tenant);

			
			Log.d(TAG, "Address in DB: " + cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS)));
			String retrievedAddress = cursor.getString(cursor.getColumnIndex(COLUMN_ADDRESS));
			//String formattedAddress = AddressFormatter.extractStreet(retrievedAddress);
			//address.setThoroughfare(formattedAddress);
			property.setAddress(AddressFormatter.buildAddressObject(retrievedAddress));

			//Loan loan = new Loan(Integer.parseInt(cursor.getString(4)));
			//property.setLoan(loan);
			property.setRent(cursor.getDouble(cursor.getColumnIndex(COLUMN_RENT)));

			Log.d(TAG + " getProperty(" + id + ")", property.getNickname());

			cursor.close();
			db.close();
			return property;
		} else {
			return null;
		}
	}

	/**
	 * Return a list of property names only
	 * @return
	 */
	public List<Property> getAllProperties() {
		List<Property> properties = new ArrayList<Property>();

		// 1. build the query
		String query = "SELECT  * FROM " + TABLE_PROPERTY;

		// 2. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);

		// 3. go over each row, build property and add it to list
		Property property = null;
		if (cursor.moveToFirst()) {
			do {
				property = new Property(cursor.getString(1));
				property.setId(cursor.getString(0));
				properties.add(property);
			} while (cursor.moveToNext());
		}

		Log.d(TAG, "getAllProperties(): # of records: " + properties.size());
		cursor.close();
		return properties;
	}
	
	/**
	 * Update tenant in property table
	 * @return number of rows affected
	 */
	public int updateTenant(String id, String name) {
		
		// 1. get reference to writable DB
	    SQLiteDatabase db = this.getWritableDatabase();
	 
	    // 2. create ContentValues to add key "column"/value
	    ContentValues values = new ContentValues();
	    values.put(COLUMN_TENANT, name); // get tenant 
	 
	    // 3. updating row
	    int i = db.update(TABLE_PROPERTY, //table
	            values, // column/value
	            COLUMN_ID +" = ?", // selections
	            new String[] { id }); //selection args
	 
	    // 4. close
	    db.close();
	 
	    return i;
		
	}

	/**
	 * Delete a property
	 * @param id
	 */
	public void deleteProperty(String id) {

		// 1. get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();

		// 2. delete
		db.delete(TABLE_PROPERTY, // table name
				COLUMN_ID + " = ?", // selections
				new String[] { id }); // selections args

		// 3. close
		db.close();

		// log
		Log.d("Deleted Property=", id);
	}
	
	/**
	 * Returns true if property exists
	 * @param id
	 * @return
	 */
	public boolean checkIfPropertyExists(String id) {
		// 1. get reference to readable DB
		SQLiteDatabase db = this.getReadableDatabase();

		// 2. build query
		Cursor cursor = db.query(TABLE_PROPERTY, // a. table
				COLUMNS, // b. column names
				" _id = ?", // c. selections
				new String[] { id }, // d. selections args
				null, // e. group by
				null, // f. having
				null, // g. order by
				null); // h. limit

		// 3. if we got results get the first one
		Log.d(TAG, "Check if property exists=" + cursor.moveToFirst());
		db.close();
		return cursor.moveToFirst();
	}
	
	/**
	 * Update a Property
	 * @param property
	 */
	public void updateProperty(Property property) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_ID, property.getId());
		values.put(COLUMN_ADDRESS, property.getAddress().toString());
		values.put(COLUMN_NICKNAME, property.getNickname().toString());
		
		Log.d(TAG, "Update Values=" + values.toString());
		db.update(TABLE_PROPERTY, values, COLUMN_ID +" = ?", new String[] { property.getId() });
		Log.d(TAG, "DB Updated");
		db.close();
	}

}
