package com.nomoreboundary.myrentalpro.property;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nomoreboundary.myrentalpro.PropertyListActivity;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Property;
import com.nomoreboundary.myrentalpro.entity.Tenant;

public class AddPropertyActivity extends Activity {

	Button mAddProperty;
	EditText mNickname;
	EditText mStreet, mUnit, mCity, mState, mZip;
	EditText mTenant;
	EditText mRent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_property);
		// Show the Up button in the action bar.
		setupActionBar();

		mAddProperty = (Button) findViewById(R.id.btnAddAProperty);
		mNickname = (EditText) findViewById(R.id.edtNickname);
		mStreet = (EditText) findViewById(R.id.edtStreet);
		mUnit = (EditText) findViewById(R.id.edtUnit);
		mCity = (EditText) findViewById(R.id.edtCity);
		mState = (EditText) findViewById(R.id.edtState);
		mZip = (EditText) findViewById(R.id.edtZip);
		mTenant = (EditText) findViewById(R.id.edtTenant);
		mRent = (EditText) findViewById(R.id.edtRent);

	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_property, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * onClickListener for addProperty button
	 * 
	 * @param view
	 */
	public void addProperty(View view) {
		View focusView = null;
		boolean cancel = false;

		// Check for a valid nickname
		String nickname = mNickname.getText().toString();
		if (TextUtils.isEmpty(nickname)) {
			mNickname.setError("Enter Name");
			focusView = mNickname;
			cancel = true;
		}

		// Street
		String street = mStreet.getText().toString();
		if (TextUtils.isEmpty(street)) {
			mStreet.setError("Enter Street");
			focusView = mStreet;
			cancel = true;
		}

		// Unit
		String unit = mUnit.getText().toString();
		if (TextUtils.isEmpty(unit)) {
			unit = "";
		}

		// City
		String city = mCity.getText().toString();
		if (TextUtils.isEmpty(city)) {
			mCity.setError("Enter City");
			focusView = mCity;
			cancel = true;
		}

		// State
		String state = mState.getText().toString();
		if (TextUtils.isEmpty(state)) {
			mState.setError("Enter State");
			focusView = mState;
			cancel = true;
		}

		// Zip
		String zip = mZip.getText().toString();
		if (TextUtils.isEmpty(zip)) {
			mZip.setError("Enter Zip");
			focusView = mZip;
			cancel = true;
		}

		// Tenant
		String tenantName = mTenant.getText().toString();
		if (TextUtils.isEmpty(tenantName)) {
			tenantName = "";
		}

		// rent
		String rent = mRent.getText().toString();
		if (TextUtils.isEmpty(rent)) {
			rent = "0";
		}

		if (cancel) {
			focusView.requestFocus();
		} else {
			Property property = new Property(nickname);

			// Set an Address
			Address address = new Address(Locale.US);
			address.setThoroughfare(street + " " + unit);
			address.setLocality(city);
			address.setAdminArea(state);
			address.setPostalCode(zip);

			property.setAddress(address);

			// Set a tenant
			Tenant tenant = new Tenant(tenantName);
			property.setTenant(tenant);

			// Set the rent
			property.setRent(Double.parseDouble(rent));

			// Add to database
			PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
			dbHelper.addProperty(property);

			// Open the Property List Activity
			Intent intent = new Intent(this, PropertyListActivity.class);
			startActivity(intent);
		}
	}
}
