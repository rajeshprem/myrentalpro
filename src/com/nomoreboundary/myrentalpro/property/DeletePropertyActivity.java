package com.nomoreboundary.myrentalpro.property;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.PropertyListActivity;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.rent.DeleteItemDialogFragment;

public class DeletePropertyActivity extends FragmentActivity implements
		DeleteItemDialogFragment.DeleteItemDialogListener {

	String mPropertyId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delete_property);
		// Show the Up button in the action bar.
		setupActionBar();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.delete_property, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void deleteProperty(View view) {
		DialogFragment removeDialogFragment = new DeleteItemDialogFragment();
		removeDialogFragment.show(getSupportFragmentManager(),
				"DeleteItemDialogFragment");
	}
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		
		// Delete Property
		PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
		dbHelper.deleteProperty(mPropertyId);
		
		// Go to Home Screen
		Intent intent = new Intent(this, PropertyListActivity.class);
		startActivity(intent);	
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// Cancel and Return to Activity
	}

}
