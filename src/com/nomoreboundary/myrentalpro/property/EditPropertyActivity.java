package com.nomoreboundary.myrentalpro.property;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nomoreboundary.myrentalpro.PropertyDetailActivity;
import com.nomoreboundary.myrentalpro.PropertyDetailFragment;
import com.nomoreboundary.myrentalpro.R;
import com.nomoreboundary.myrentalpro.data.PropertyDatabaseHelper;
import com.nomoreboundary.myrentalpro.entity.Property;

public class EditPropertyActivity extends Activity {

	private static final String TAG = "EditPropertyActivity";
	String mPropertyId;
	EditText mNickname, mStreet, mCity, mUnit, mZip, mState;
	
	private Property mProperty;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_property);
		// Show the Up button in the action bar.
		setupActionBar();

		// Grab property ID from bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mPropertyId = extras.getString("property_id");
		} else {
			Toast.makeText(this, "Could not get Property ID.",
					Toast.LENGTH_SHORT).show();
		}
		
		mNickname = (EditText) findViewById(R.id.edtPropertyNickname);
		mStreet = (EditText) findViewById(R.id.edtPropertyStreet);
		mCity = (EditText) findViewById(R.id.edtPropertyCity);
		mUnit = (EditText) findViewById(R.id.edtPropertyUnit);
		mZip = (EditText) findViewById(R.id.edtPropertyZip);
		mState = (EditText) findViewById(R.id.edtPropertyState);
		
		updatePropertyFields();
	}
	
	private void updatePropertyFields() {
		PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);

		if (mPropertyId != null) {
			Property property = dbHelper.getProperty(mPropertyId);
			if (property != null) {
				mNickname.setText(property.getNickname());

				Address address = property.getAddress();

				// Split unit #
				String fullStreet = address.getThoroughfare();
				List<String> streetSplit = Arrays.asList(fullStreet.split(" "));
				int sz = streetSplit.size();

				String str = "";
				if (!streetSplit.isEmpty()) {
					mUnit.setText(streetSplit.get(sz - 1));
					for (int i = 0; i < sz - 1 ; i++) {
						str += streetSplit.get(i) + " ";
					}
					mStreet.setText(str);
				}

				mCity.setText(address.getLocality());
				mZip.setText(address.getPostalCode());
				mState.setText(address.getAdminArea());
				Log.d(TAG, "updatePropertyFields(): Update all edit fields");
			} else {
				Log.d(TAG, "updatePropertyFields(): Nothing to update");
			}
		} else {
			Log.d(TAG, "updatePropertyFields():  mPropertyId=" + mPropertyId);
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_property, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			upIntent.putExtra(PropertyDetailFragment.ARG_ITEM_ID, mPropertyId);

			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
						.addNextIntentWithParentStack(upIntent)
						.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void parseInputParameters(){
		Property property = new Property();
		property.setId(mPropertyId);
		property.setNickname(mNickname.getText().toString());
		
		Address address = new Address(Locale.US);
		address.setThoroughfare(mStreet.getText().toString() + " " + mUnit.getText().toString());  //holds unit info
		address.setAdminArea(mState.getText().toString());
		address.setPostalCode(mZip.getText().toString());
		address.setLocality(mCity.getText().toString());
		property.setAddress(address);

		this.mProperty = property;
	}
	
	/**
	 * On click listener for Update property
	 * @param view
	 */
	public void updateProperty(View view)
	{
		PropertyDatabaseHelper dbHelper = new PropertyDatabaseHelper(this);
		
		parseInputParameters();
		
		if (mProperty != null && mPropertyId != null) {
			if (dbHelper.checkIfPropertyExists(mPropertyId)) {
				dbHelper.updateProperty(mProperty);  // update only address details
				Log.d(TAG, "Loan UPDATED for propertyId=" + mPropertyId);
			} else {
				Log.e(TAG, "Property does NOT exist. propertyId=" + mPropertyId);
			}
			updatePropertyFields();
		} else {
			Log.d(TAG, "propertyId=" + mPropertyId);
		}
		
		// After update go back to Property Detail Activity
		Intent intent = new Intent(this, PropertyDetailActivity.class);
		intent.putExtra("property_id", mPropertyId);
		startActivity(intent);
	}

}
